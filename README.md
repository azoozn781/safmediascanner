# SAF Media Scanner
<img alt="Logo" src="app/src/main/res/mipmap-xxxhdpi/ic_launcher.png" width="80">

A plain audio file metadata extractor ("scanner") especially for **classical music**. It basically creates SQLite databases to be used by player applications like the *Unpopular Music Player* or the *Opus 1 Music Player*.

Contrary to the *Classical Music Scanner*, the *SAF Media Scanner* exclusively uses Google's *Storage Access Framework* (SAF) instead of the standard *File* scheme. Additionally the scanner copies, if possible, the resulting database file to the respective directory, in its dedicated subdirectory *ClassicalMusicDb*. If this is not possible, it remains in internal memory and kept there, with an informative file name that allows later to derive the respective scanned path. 

## Technical Background

By design any file access via Google's SAF is extremely slow compared to the standard methods (either via Java or native code). Thus it is recommended to use SAF only if necessary. Generally there has been much discussion and critics in the Android developer community complaining about Google's decisions and restrictions.

As SAF unfortunately is exclusively available in Java, this program is also forced to use slow Java code where the *Classical Music Scanner* runs in native (written in 'C') code.

## Application Field

* The SAF scanner can access USB memory devices, like USB sticks via OTG adapter, in Android 6, 7 and 8. Note that Android 9 and 10 natively allow (read) file access to USB memory, just like to SD cards.
* Note that without SAF and without manage files permission, Android 11 and newer deny any access to non-private memory, except media files.
* The SAF scanner can extract album art on SD cards and USB memory devices, this is currently not possible with the classical scanner, because without SAF it is limited to read-only access.
* The SAF scanner can access SMB or SFTP servers, if the respective document provider is installed.
* The SAF scanner even deals with non-seekable files by reading them completely into memory before further processing. 

## Remark

As "pop music" is an abbreviation for "popular music", any other music obviously is "unpopular", so this program could also be called *Unpopular Music Scanner*.

***

<a href='https://play.google.com/store/apps/details?id=de.kromke.andreas.safmediascanner'><img src='public/google-play.png' alt='Get it on Google Play' height=45/></a>
<a href='https://f-droid.org/app/de.kromke.andreas.safmediascanner'><img src='public/f-droid.png' alt='Get it on F-Droid' height=45 ></a>

***

# Music Organisation

Other music organisation apps are meant for pop music only and deal with "Songs" and "Artists". This is not suitable for classical (baroque, renaissance, ...) music which is organised by composer, work, movements and performers.

Pop music:

Artist | Album  | Song
-------| ------ | -------------
Smith | Heaven  | 1. Love you, babe
 | | 2. Babe, love you
 | | 3. More love, babe, inne wadr
Jones  | Freedom | 1. Babe, I'm free
 | | 2. Free the innrnatt

Classical music needs more metadata ("tags"), especially the *composer tag* and *grouping tag*, the latter is used to group the movements together:

| Album  | Composer  | Work | Performer | Movement
|------- |-----------| -----| --- | ---
| Cello Concertos | Joseph Haydn | Concerto No. 1 | Fred Stone | 1. Allegro
|                 |              |                |            | 2. Adagio 
|                 |              |                |            | 3. Presto 
|                 | Hella Haydn  | Concerto No. 3 | Jean Water | 1. Allegro
|                 |              |                |            | 2. Adagio


For more information see the Google Play Store entry or the offline help text of the application (if existing). Also consult the *Unpopular Music Player*.

# Screenshots
<img alt="No" src="fastlane/metadata/android/en-US/images/phoneScreenshots/Screenshot_1.png" width="320">
<img alt="No" src="fastlane/metadata/android/en-US/images/phoneScreenshots/Screenshot_2.png" width="320">
<img alt="No" src="fastlane/metadata/android/en-US/images/phoneScreenshots/Screenshot_3.png" width="320">


# Supported

* To be run manually whenever audio files have been added, removed or changed.
* Auto scan (incremental) or complete scan (rebuild) selectable.
* Various audio file types (mp3, mp4, flac, ogg, opus, ...).
* Multi CD albums: sub-subdirectories "CD1", "CD2", ..., with common folder image.
* Common and non-common text tags.
* Composer and grouping (work, movements).
* Proprietary Apple iTunes tags for classical music.
* Extracting of embedded images if none already exists.
* Downscaling of both extracted and existing album images.
* Original images can be kept as backup, if desired.
* Maximum image size is configurable.
* Creates a standard SQLite database in "(...)/ClassicalMusicDb" that can be accessed from PC or other apps.
* Uses own, compact Java tag reading library or native code (C++) tagger library.

# Not Supported (Yet)

* File system change monitoring
* Exotic album picture file names like Folder.jpg or FOLDER.JPG or Cover_front.jpg etc.

# Permissions Needed

* Read storage
* Write storage

# License

The SAF Media Scanner is licensed according to GPLv3, see LICENSE file.

# External Licenses

**TagLib C++ Library:**  
Source: https://taglib.org/  
License: LGPL (http://www.gnu.org/copyleft/lesser.html) and MPL (https://www.mozilla.org/en-US/MPL/1.1/)
