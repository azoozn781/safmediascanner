//
// Created by Andreas on 13.06.20.
//

#include <assert.h>
#include "jni-utils.h"


/******************************************************************************
 *
 * Set value of Java string in calling Java object
 *
 *****************************************************************************/

extern void jniSetString
(
    JNIEnv* env,
    jobject thisObj,
    const char *javaMemberName,
    const char *newValue
)
{
    jfieldID fid;
    jstring jString;

    jclass thisClass = env->GetObjectClass(thisObj);
    fid = env->GetFieldID(thisClass, javaMemberName, "Ljava/lang/String;");
    assert(fid != nullptr);
    /* get the old value:
    jstring prevVal = (jstring) env->GetObjectField(thisObj, fid);
    const char *cStr = env->GetStringUTFChars(prevVal, nullptr);
    assert(cStr != nullptr);
    env->ReleaseStringUTFChars(prevVal, cStr);
    */
    jString = env->NewStringUTF(newValue);
    assert((newValue == nullptr) || (jString != nullptr));
    env->SetObjectField(thisObj, fid, jString);
}


/******************************************************************************
 *
 * Set value of Java "int" in calling Java object
 *
 *****************************************************************************/

void jniSetInt(JNIEnv* env, jobject thisObj, const char *javaMemberName, int newValue)
{
    jfieldID fid;

    jclass thisClass = env->GetObjectClass(thisObj);
    fid = env->GetFieldID(thisClass, javaMemberName, "I");
    assert(fid != nullptr);
    /* get the old value:
    jint prevVal = (int) env->GetIntField(thisObj, fid);
    */
    env->SetIntField(thisObj, fid, newValue);
}
