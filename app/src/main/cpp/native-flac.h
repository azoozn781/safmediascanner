/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef SAFMEDIASCANNER_NATIVE_FLAC_H
#define SAFMEDIASCANNER_NATIVE_FLAC_H

#include "flacpicture.h"

extern void handleFlac(audioFileInfo *info, const char *path, int fd, long length);
extern void extractPictureFlac(int pic_fd, const char *path, int fd, long length);
extern void handlePictureFlac(audioFileInfo *info, int pic_fd, const TagLib::List<TagLib::FLAC::Picture*>& picList);

#endif //SAFMEDIASCANNER_NATIVE_FLAC_H
