/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MEDIASCANNER_NATIVE_LIB_UTILS_H
#define MEDIASCANNER_NATIVE_LIB_UTILS_H

#define MAXSTR 512          // ID strings
#define MAXPATH 255
#define MAXFNAME 255

#define SKIP_SPACE(s) while (*(s) == ' ' || *(s) == '\t') (s)++;
#define MAX(a,b) (((a) >= (b)) ? (a) : (b));

extern void strCpySafe(char *dst, const char *src);
//extern int strCmpSafe(const char *s1, const char *s2);
extern void strCpyUnique(char *dst, const char *src);
extern const char *copyNonEmptyString(const char *s, const char *dotpos = nullptr);
extern int fromLong(long l);
extern int evalNfromM(int *n, int *m, const char *s);

extern const char * getFileNameFromPath(const char *path);
extern void getDirNameFromPath(const char *path, long l, int *start_index, int *stop_index);
extern void extractDirNameFromPath
(
    const char *path,       // src
    long lp,                // length of path
    char *fname,            // dst
    const char **startp     // out: pointer to fname inside path
);
extern int checkDirNameIsCdNumber(const char *dirname, int l);

static inline bool strZeroOrEmpty(const char *s)
{
    return (s == nullptr) || (*s == '\0');
}

int readFileOfUnknownLength(int fd, char **pbuf, long *len);

#endif //MEDIASCANNER_NATIVE_LIB_UTILS_H
