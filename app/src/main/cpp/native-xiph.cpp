/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

//#include <string>
#include <dirent.h>
#include <cerrno>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cinttypes>
#include <stdio.h>

// Android
#include <android/log.h>
#include <ogg/xiphcomment.h>

#include "native-lib.h"
#include "native-lib-utils.h"

#include "native-xiph.h"


/************************************************************************************
 *
 * flac helper
 *
 ***********************************************************************************/
static void getOggTag
(
    audioFileInfo *info,
    const TagLib::Ogg::FieldListMap *map,
    const char *key,
    const char *keyname,
    int tagno
)
{
    (void) keyname;     // only for debugging purposes
    if (map->contains(key))
    {
        TagLib::StringList sl = (*map)[key];
        unsigned n = sl.size();
        for (unsigned i = 0; i < n; i++)
        {
            TagLib::String cs = sl[i];
            const char *s = cs.toCString(true /* unicode */);
            LOGD("   %-40s is \"%s\"\n", keyname, s);
            if ((i == 0) && (tagno >= 0))
            {
                info->tags[tagno] = copyNonEmptyString(s);
            }
        }
    }
}


/************************************************************************************
 *
 * advanced ogg handling (vorbis and flac)
 *
 ***********************************************************************************/
void handleOgg(audioFileInfo *info, TagLib::Ogg::XiphComment *tag)
{
    const TagLib::Ogg::FieldListMap &map = tag->fieldListMap();

    getOggTag(info, &map, "ALBUM", "album", idAlbum);
    getOggTag(info, &map, "ARTIST", "artist (i.e. performer)", idArtist);
    getOggTag(info, &map, "ALBUMARTIST", "album artist (i.e. album performer)", idAlbumArtist);
    getOggTag(info, &map, "CONDUCTOR", "conductor", idConductor);
    getOggTag(info, &map, "GROUPING", "grouping (i.e. work title)", idGrouping);
    getOggTag(info, &map, "TITLE", "title (i.e. movement title)", idTitle);
    getOggTag(info, &map, "SUBTITLE", "subtitle (e.g. opus number)", idSubtitle);
    getOggTag(info, &map, "COMPOSER", "composer", idComposer);
    getOggTag(info, &map, "TRACKNUMBER", "track number / number of tracks", idTrack);
    getOggTag(info, &map, "DISCNUMBER", "disk number / number of disks", idDiscNo);
    getOggTag(info, &map, "GENRE", "genre", idGenre);
    getOggTag(info, &map, "COMMENT", "comment", idComment);
    getOggTag(info, &map, "ORIGINALDATE", "original year", -1);
    getOggTag(info, &map, "DATE", "year", idYear);
    // MusicBrainz Picard compatible movement tags
    getOggTag(info, &map, "MOVEMENTNAME", "Apple movement", idAppleMovement);
    getOggTag(info, &map, "MOVEMENT", "Apple movement number",idAppleMovementNo);
    getOggTag(info, &map, "MOVEMENTTOTAL", "Apple number of movements", idAppleMovementTotal);
    getOggTag(info, &map, "WORK", "Apple work", idAppleWork);
}
