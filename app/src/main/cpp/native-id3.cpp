/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

//#include <string>
#include <dirent.h>
#include <cerrno>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cinttypes>
#include <stdio.h>

// Android
#include <android/log.h>
#include <mpeg/id3v1/id3v1tag.h>
#include <mpeg/id3v2/id3v2tag.h>
#include <mpeg/id3v2/frames/attachedpictureframe.h>

#include "native-lib.h"
#include "native-lib-utils.h"
#include "native-tag-utils.h"
#include "native-id3.h"


/************************************************************************************
 *
 * mp3 helper
 *
 ***********************************************************************************/
static void getMp3v2Tag
(
    audioFileInfo *info,
    const TagLib::ID3v2::FrameListMap *map,
    const char *key,
    const char *keyname,
    int tagno
)
{
    (void) keyname;     // only for debugging purposes
    // Get the list of frames for a specific frame type
    TagLib::ID3v2::FrameList l = (*map)[key];
    if (!l.isEmpty())
    {
        // call front() to get only the first value. Ignore others?!?
#if 0
        // fails with corrupted string
        const char *s = l.front()->toString().toCString(true /* unicode */);
#else
        const TagLib::ID3v2::Frame * frame = l.front();
        TagLib::String str = frame->toString();                 // this object must be "alive" as long we need the text
        const char *s = str.toCString(true /* unicode */);
#endif
        LOGD("   %-40s is \"%s\"\n", keyname, s);
        if (tagno >= 0)
        {
            info->tags[tagno] = copyNonEmptyString(s);
        }
#if 0
        // debug code
        if (info->id == 11)
        {
            LOGE("getMp3Tag:   %-40s is \"%s\"\n", keyname, s);
        }
#endif
    }
    else
    {
        info->tags[tagno] = nullptr;
    }
}


/************************************************************************************
 *
 * advanced id3v1 handling
 *
 ***********************************************************************************/
void handleId3v1(audioFileInfo *info, const TagLib::ID3v1::Tag *tag)
{
    LOGD_ID3V1("NOTICE: mp3v1 handled");

    TagLib::String str;
    const char *s;
    char buf[32];

    str = tag->album();
    s = str.toCString(true /* unicode */);
    info->tags[idAlbum] = copyNonEmptyString(s);

    str = tag->artist();
    s = str.toCString(true /* unicode */);
    info->tags[idArtist] = copyNonEmptyString(s);

    str = tag->title();
    s = str.toCString(true /* unicode */);
    info->tags[idTitle] = copyNonEmptyString(s);

    sprintf(buf, "%d", tag->track());
    info->tags[idTrack] = copyNonEmptyString(buf);

    str = tag->genre();
    s = str.toCString(true /* unicode */);
    info->tags[idGenre] = copyNonEmptyString(s);

    str = tag->comment();
    s = str.toCString(true /* unicode */);
    info->tags[idComment] = copyNonEmptyString(s);

    sprintf(buf, "%d", tag->year());
    info->tags[idYear] = copyNonEmptyString(buf);
}


/************************************************************************************
 *
 * mp3 helper
 *
 ***********************************************************************************/
static void getMp3TagWithMultipleValues
(
    audioFileInfo *info,
    int pic_fd,             // -1: do not extract
    const TagLib::ID3v2::FrameListMap *map,
    const char *key,
    const char *keyname,
    int tagno
)
{
    (void) keyname;     // only for debugging purposes
    // Get the list of frames for a specific frame type
    TagLib::ID3v2::FrameList l = (*map)[key];
    bool bIsPicture = !strcmp(key, "APIC");
    if (!l.isEmpty())
    {
        // iterate through all values for this key
        for (TagLib::ID3v2::FrameList::ConstIterator it = l.begin(); it != l.end() ; it++)
        {
            if (bIsPicture)
            {
                // strange method to cast general Frame to AttachedPictureFrame
                auto pic = dynamic_cast<TagLib::ID3v2::AttachedPictureFrame *> (*it);

                TagLib::String str_txt = pic->toString();
                TagLib::String str_mime = pic->mimeType();
                TagLib::String str_desc = pic->description();

                /*
                const char *txt  = str_txt.toCString(true);
                const char *desc = str_desc.toCString(true);
                LOGD("   picture text is \"%s\"\n", txt);
                LOGD("   picture description is \"%s\"\n", desc);
                */

                const char *mime = str_mime.toCString(true /* unicode */);
                int type = pic->type();
                TagLib::ByteVector data = pic->picture();
                int picsize = data.size();
                LOGD("   picture mime type is \"%s\"\n", mime);
                LOGD("   picture type is %d\n", type);
                LOGD("   data size is %d\n", picsize);

                bool bDone = handleIfCoverPicture(info, pic_fd, type, picsize, TagLib::ID3v2::AttachedPictureFrame::FrontCover, mime, &data);
                if (bDone)
                {
                    return;
                }
            }
            else
            {
                const TagLib::ID3v2::Frame * frame = l.front();
                TagLib::String str = frame->toString();                 // this object must be "alive" as long we need the text
                const char *s = str.toCString(true /* unicode */);
                LOGD("   %-40s is \"%s\"\n", keyname, s);
                if (tagno >= 0)
                {
                    info->tags[tagno] = copyNonEmptyString(s);
                }
            }
        }
    }
    else
    {
        info->tags[tagno] = nullptr;
    }
}


/************************************************************************************
 *
 * advanced id3v2 handling
 *
 ***********************************************************************************/
void handleId3v2(audioFileInfo *info, const TagLib::ID3v2::Tag *tag)
{
    if (tag != nullptr)
    {
        const TagLib::ID3v2::FrameListMap &map = tag->frameListMap();

        getMp3v2Tag(info, &map, "TALB", "album", idAlbum);
        getMp3v2Tag(info, &map, "TPE1", "artist (i.e. performer)", idArtist);
        getMp3v2Tag(info, &map, "TPE2", "album artist (i.e. album performer)", idAlbumArtist);
        getMp3v2Tag(info, &map, "TPE3", "conductor", idConductor);
        getMp3v2Tag(info, &map, "TIT1", "grouping (i.e. work title)", idGrouping);
        getMp3v2Tag(info, &map, "TIT2", "title (i.e. movement title)", idTitle);
        getMp3v2Tag(info, &map, "TIT3", "subtitle (e.g. opus number)", idSubtitle);
        getMp3v2Tag(info, &map, "TCOM", "composer", idComposer);
        getMp3v2Tag(info, &map, "TRCK", "track number / number of tracks", idTrack);
        getMp3v2Tag(info, &map, "TPOS", "disk number / number of disks", idDiscNo);
        //getMp3v2Tag(c, &map, "TCON", "genre", idGenre);             // WARNING: taglib in updateGenre() internally converts "(101)" to "101"!
        getMp3v2Tag(info, &map, "COMM", "comment", idComment);
        getMp3v2Tag(info, &map, "TORY", "original year", -1);
        getMp3v2Tag(info, &map, "TDRC", "year", idYear);               // WARNING: do not use TYER, as taglib internally converts v2.3 to v2.4
        getMp3v2Tag(info, &map, "MVNM", "Apple movement", idAppleMovement);
        getMp3v2Tag(info, &map, "MVIN", "Apple movement number / number of movements", idAppleMovementNo);
        getMp3v2Tag(info, &map, "GRP1", "Apple work", idAppleMp3Work);
        getMp3TagWithMultipleValues(info, -1, &map, "APIC", "pictures", -1);

        // special
        TagLib::String str = tag->genre();
        if (!str.isEmpty())
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"
            const char *key = "(genre)";
            const char *keyname = "genre";
#pragma clang diagnostic pop
            int tagno = idGenre;

            const char *s = str.toCString(true /* unicode */);
            LOGD("   %-40s is \"%s\"\n", keyname, s);
            info->tags[tagno] = copyNonEmptyString(s);
        }

        // derived
        info->tags[idAppleWork] = info->tags[idAppleMp3Work];
    }
}


/************************************************************************************
 *
 * used for any files with id3v2 tags
 *
 ***********************************************************************************/
void extractPictureId3v2(audioFileInfo *info, int pic_fd, const TagLib::ID3v2::Tag *tag)
{
    const TagLib::ID3v2::FrameListMap &map = tag->frameListMap();
    getMp3TagWithMultipleValues(info, pic_fd, &map, "APIC", "pictures", -1);
}
