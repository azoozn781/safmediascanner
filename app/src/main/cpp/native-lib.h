/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Java/JNI
#include <jni.h>
#include <toolkit/tiostream.h>
#include <audioproperties.h>

#define MAXSTR 512          // ID strings
#define MAXPATH 255
#define MAXFNAME 255

#define LOG_TAG    "native-lib"
#define NUMELEMS(a) (sizeof(a)/sizeof((a)[0]))
#define SKIP_SPACE(s) while (*(s) == ' ' || *(s) == '\t') (s)++;

#define xstr(s) str(s)
#define str(s) #s

#if !defined(LOG_LEVEL)
#define LOG_LEVEL 0
#endif

#if LOG_LEVEL > 2
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#else
#define  LOGD(...)
#endif
#if LOG_LEVEL > 1
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#else
#define  LOGI(...)
#endif
#if LOG_LEVEL > 0
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#else
#define  LOGW(...)
#endif
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#if LOG_LEVEL > 0
#define  LOGD_ID3V1(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#else
#define  LOGD_ID3V1(...)
#endif

typedef enum
{
    eMp3 = 0,
    eMp4 = 1,
    eOgg = 2,
    eFlac = 3,
    eOpus = 4,
    eInvalid = -1
} AudioFileType;


enum tagType
{
    eTagUnknown = 0,
    eTagId3v1 = 1,
    eTagId3v2 = 2,
    eTagMp4 = 10,
    eTagOgg = 11
};


enum tagNumbers
{
    idAlbum = 0,                // mp3: TALB    mp4: ©alb       flac: ALBUM
    idArtist = 1,               // mp3: TPE1    mp4: ©ART       flac: ARTIST
    idAlbumArtist = 2,          // mp3: TPE2    mp4: aART       flac: ALBUMARTIST
    idConductor = 3,            // mp3: TPE3    mp4: com.apple.iTunes.CONDUCTOR     (should be ©con?)
    idGrouping = 4,             // mp3: TIT1    mp4: ©grp       flac: GROUPING
    idTitle = 5,                // mp3: TIT2    mp4: ©nam       flac: TITLE
    idSubtitle = 6,             // mp3: TIT3    mp4: com.apple.iTunes.SUBTITLE      (should be desc)
    idComposer = 7,             // mp3: TCOM    mp4: ©wrt       flac: COMPOSER
    idDiscNo = 8,               // mp3: TPOS    mp4: disk       flac: DISCNUMBER
//  idDiscTotal = 9,            // mp3: TPOS    mp4: disk
    idTrack = 10,               // mp3: TRCK    mp4: trkn       flac: TRACKNUMBER
//  idTrackTotal = 11,          // mp3: TRCK    mp4: trkn
    idGenre = 12,               // mp3: TCON    mp4: ©gen       flac: GENRE
    idYear = 13,                // mp3: TYER    mp4: ©day       flac: DATE
    idComment = 14,             // mp3: COMM    mp4: ©cmt       flac: COMMENT
// iTunes:
    idAppleMovement = 15,       // mp3: MVNM   mp4: ©mvn
    idAppleMovementNo = 16,     // mp3: MVIN   mp4: ©mvi
    idAppleMovementTotal = 17,  // mp3: MVIN   mp4: ©mvc
    idAppleMp3Work = 18,        // mp3: GRP1   mp4: <-->
    idAppleMp4Work = 19,        // mp3: <-->   mp4: ©wrk
// derived fields
    idAppleWork = 20,           // mp3: GRP1   mp4: ©wrk
    idCombinedMovement = 21,    // combine movement or title information
// number of fields
    idNum = 22
};


typedef struct
{
    // to audio file database
//  int64_t id;
    int track_disc_no;          // 1000 * discno + trackno
    const char *title;
    const char *album;
//  int64_t album_id;
    int duration;
    const char *grouping;
    const char *subtitle;
    const char *composer;
    const char *performer;
    const char *album_artist;
    const char *conductor;
    const char *genre;
    int year;
    const char *path;

    // intermediates
    int track_no;
    int disc_no;

    // for album database
    int no_tracks;
    int no_discs;

    // raw text tags
    const char *tags[idNum];    // raw

    // picture (album art)
    int pic_type;               // 0: none, 1: jpeg, 2: png
    int pic_size;

    // file information
    time_t mtime;               // file modification time

    // debug
    tagType tag_type;
} audioFileInfo;



// final result passed to callback
typedef enum
{
    eResultMalformedPath = -4,
    eResultSqlError = -3,
    eResultCannotOpenDb = -2,
    eResultInvalidCmd = -1,
    eResultProgress = 0,
    eResultFileScan = 1,
    eResultFileProcess = 2,
    eResultDbCreatedOrOpened = 3,
    eResultDone = 4,
    eResultAlbumScan = 5,
    eResultTablesRemoved = 6,
    eResultAlbumPicturesUpdated = 7
} resultType;

void handleProps(audioFileInfo *info, TagLib::AudioProperties *props);
TagLib::IOStream *createIOStream(const char *path, int fd, long length, bool bReadOnly);
