#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-macro-usage"
#pragma ide diagnostic ignored "cppcoreguidelines-avoid-goto"
/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

//#include <string>
#include <dirent.h>
#include <cerrno>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cinttypes>
#include <stdio.h>

// Android
#include <android/log.h>

#include "native-lib.h"
#include "native-mp3.h"
#include "native-cmd.h"

// advanced taglib interface
#include <mpeg/id3v2/id3v2tag.h>
#include <mpeg/id3v1/id3v1tag.h>
#include <mpeg/id3v2/frames/attachedpictureframe.h>
#include "mpegfile.h"
#include <mp4/mp4file.h>
#include <ogg/flac/oggflacfile.h>
#include <ogg/vorbis/vorbisfile.h>
#include <flac/flacfile.h>

#include "native-lib-utils.h"
#include "native-tag-utils.h"
#include "native-id3.h"


/************************************************************************************
 *
 * advanced mp3 handling
 *
 ***********************************************************************************/
void handleMp3(audioFileInfo *info, const char *path, int fd, long length)
{
    TagLib::IOStream *stream = createIOStream(path, fd, length, true);
    TagLib::MPEG::File f(stream, TagLib::ID3v2::FrameFactory::instance());
    TagLib::AudioProperties *props = f.audioProperties();
    handleProps(info, props);

    // Check to make sure that it has an ID3v2 tag
    TagLib::ID3v2::Tag *tag = f.ID3v2Tag();
    if ((tag != nullptr) && !tag->isEmpty())
    {
        handleId3v2(info, tag);
        info->tag_type = eTagId3v2;
    }
    else
    {
        // try ID3v1 tag
        TagLib::ID3v1::Tag *tag1 = f.ID3v1Tag();
        if (tag1 != nullptr)
        {
            handleId3v1(info, tag1);
            info->tag_type = eTagId3v1;
        }
        else
        {
            info->tag_type = eTagUnknown;
        }
    }
}


/************************************************************************************
 *
 * extract picture from mp3 file
 *
 ***********************************************************************************/
void extractPictureMp3(int pic_fd, const char *path, int fd, long length)
{
    TagLib::IOStream *stream = createIOStream(path, fd, length, true);
    TagLib::MPEG::File f(stream, TagLib::ID3v2::FrameFactory::instance());
    TagLib::ID3v2::Tag *tag = f.ID3v2Tag();
    if (tag != nullptr)
    {
        audioFileInfo info;
        extractPictureId3v2(&info, pic_fd, tag);
    }
}


#pragma clang diagnostic pop
