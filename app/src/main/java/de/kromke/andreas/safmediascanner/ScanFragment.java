/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.safmediascanner;

import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

@SuppressWarnings("Convert2Lambda")
public class ScanFragment extends Fragment
{
    private static final String LOG_TAG = "SMS : ScanFragment";
    private TextView mScanTextView;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    )
    {
        Log.d(LOG_TAG, "onCreateView()");
        MainActivity currActivity = getMainActivity();
        currActivity.onChangedFragment(2);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_scan, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState)
    {
        Log.d(LOG_TAG, "onViewCreated()");
        super.onViewCreated(view, savedInstanceState);

        mScanTextView = view.findViewById(R.id.textScan);
        mScanTextView.setMovementMethod(new ScrollingMovementMethod());
        MainActivity activity = getMainActivity();
        String text = "";
        if (activity != null)
        {
            Uri uri = activity.getSelectedUri();
            if (uri != null)
            {
                text = uri.getPath();
            }
        }
        text += "\n\nPRESS MAGNIFIER BUTTON TO START SCANNING\n";
        text += "PRESS TRIANGLE BUTTON FOR INCREMENTAL SCAN\n";
        mScanTextView.setText(text);

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                NavHostFragment.findNavController(ScanFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }


    /************************************************************************************
     *
     * helper
     *
     ***********************************************************************************/
    private MainActivity getMainActivity()
    {
        return (MainActivity) getActivity();
    }


    /************************************************************************************
     *
     * helper
     *
     ***********************************************************************************/
    void setScanText(final String text)
    {
        if (mScanTextView != null)
        {
            mScanTextView.setText(text);
            final int scrollAmount = mScanTextView.getLayout().getLineTop(mScanTextView.getLineCount()) - mScanTextView.getHeight();
            mScanTextView.scrollTo(0, Math.max(scrollAmount, 0));
        }
    }
}
