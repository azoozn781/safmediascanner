/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.safmediascanner;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.UriPermission;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.UriPermission.INVALID_TIME;
import static android.os.Environment.getExternalStorageDirectory;
import static android.os.Environment.getExternalStorageState;
import static de.kromke.andreas.safmediascanner.UserSettings.PREF_SHARED_DB_DIRECTORY_URI;
import static java.lang.System.identityHashCode;

@SuppressWarnings("Convert2Lambda")
public class MainActivity extends AppCompatActivity
{
    private static final String LOG_TAG = "SMS : MainActivity";
    public static final String sDbPath = "ClassicalMusicDb";
    public static final String sDbName = "safmetadata.db";
    private Uri mSharedDbPathUri = null;
    private static final int maxNumOfPersistedPermissions = 42;
    //protected static final int REQUEST_DIRECTORY_SELECT = 5;
    //protected static final int REQUEST_SHARED_DB_DIRECTORY_SELECT = 6;
    //protected static final int RESULT_SETTINGS = 1;
    private final static int MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE = 11;
    private static ColorStateList sSavedFloatingButtonColours = null;
    private static Drawable sSavedFloatingButtonLeftDrawable = null;
    private static Drawable sSavedFloatingButtonRightDrawable = null;

    private final static int minimumFolderImageSize = 256;
    private final static int defaultFolderImageSize = 500;

    boolean mReadWritePermissionGranted = false;
    List<UriPermission> mPermissionList;
    public ArrayList<String> mPathArrayList = null;
    public int mSelectedIndex = -1;

    private int mCurrFragmentNo = -1;
    private int mRequestedFragmentNo = -1;
    private SafScanner mSafScanner = null;

    private MenuItem mMenuItemSimulate;

    FloatingActionButton mFabRight;
    FloatingActionButton mFabLeft;

    Timer mTimer;
    MyTimerTask mTimerTask;
    private static final int timerFrequency = 500;         // milliseconds
    private AlertDialog mPendingDialog = null;       // test code
    public String mUniqueHash;   // for debugging purposes, to check if there are multiple instances
    public static MainActivity sInstance = null;    // HACK

    ActivityResultLauncher<Intent> mPreferencesActivityLauncher;
    ActivityResultLauncher<Intent> mRequestDirectorySelectActivityLauncher;
    ActivityResultLauncher<Intent> mRequestSharedDbDirectorySelectActivityLauncher;


    /************************************************************************************
     *
     * Activity method
     *
     * The app does _NOT_ apply for unrestricted access to all files. If to do so,
     * for MANAGE_EXTERNAL_STORAGE, see:
     * https://developer.android.com/training/data-storage/manage-all-files
     *
     ***********************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        mUniqueHash = String.format("0x%08x", identityHashCode(this));
        Log.d(LOG_TAG, "onCreate(" + mUniqueHash + ") -- API level = " + Build.VERSION.SDK_INT);

        sInstance = this;       // HACK
        super.onCreate(savedInstanceState);
        registerPreferencesCallback();
        registerDirectorySelectCallback();
        registerSharedDbDirectorySelectCallback();
        UserSettings.setContext(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);     // TODO: allow

        initPermissionsList();
        Log.d(LOG_TAG, "onCreate() -- set view");
        setContentView(R.layout.activity_main);     // creates fragment, indirectly calls onChangedFragment()
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFabLeft = findViewById(R.id.fab_left);
        mFabRight = findViewById(R.id.fab);

        if (sSavedFloatingButtonColours == null)
        {
            sSavedFloatingButtonColours = mFabLeft.getBackgroundTintList();
            sSavedFloatingButtonLeftDrawable = mFabLeft.getDrawable();
            sSavedFloatingButtonRightDrawable = mFabRight.getDrawable();
        }

        // hack for strange API 30 behaviour (onChangedFragment() called during super.onCreate())
        if (mRequestedFragmentNo != mCurrFragmentNo)
        {
            onChangedFragment(mRequestedFragmentNo);
        }

        if (!mReadWritePermissionGranted)
        {
            askPermission();
        }

        if (mSharedDbPathUri == null)
        {
            dialogLocateSharedDbDirectory();
        }

        mFabRight.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (mCurrFragmentNo == 1)
                {
                    Intent intent = createSafPickerIntent();
                    mRequestDirectorySelectActivityLauncher.launch(intent);
                }
                else
                if (mReadWritePermissionGranted)
                {
                    runScanner(false);
                }
                else
                {
                    Snackbar.make(view, "NO READ/WRITE PERMISSION", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });

        setLeftFabState(false);
        mFabLeft.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (mCurrFragmentNo == 1)
                {
                    Uri uri = getSelectedUri();
                    if (uri != null)
                    {
                        // remove from list
                        getContentResolver().releasePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        // re-read
                        initPermissionsList();
                        // tell fragment to redraw
                        PathTableFragment frag = getPathTableFragment();
                        if (frag != null)
                        {
                            frag.onChangedPathList(mPathArrayList, mSelectedIndex);
                        }
                    }
                }
                else
                if (mReadWritePermissionGranted)
                {
                    runScanner(true);
                }
                else
                {
                    Snackbar.make(view, "NO READ/WRITE PERMISSION", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });

        Log.d(LOG_TAG, "onCreate(" + mUniqueHash + ") -- done");
    }


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @Override
    protected void onStart()
    {
        mTimerTask = new MyTimerTask();
        //delay 1000ms, repeat in <timerFrequency>ms
        mTimer = new Timer();
        mTimer.schedule(mTimerTask, 1000, timerFrequency);
        super.onStart();
    }


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @Override
    protected void onStop()
    {
        // stop timer
        mTimer.cancel();    // note that a cancelled timer cannot be re-scheduled. Why not?
        mTimer = null;
        mTimerTask = null;

        super.onStop();
    }


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @Override
    protected void onDestroy()
    {
        Log.d(LOG_TAG, "onDestroy(" + mUniqueHash + ")");

        if (mPendingDialog != null)
        {
            if (mPendingDialog.isShowing())
            {
                mPendingDialog.dismiss();
            }
            mPendingDialog = null;
        }

        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy() - done");
    }


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        mMenuItemSimulate = menu.findItem(R.id.check_simulate_deleted_or_changed);
        return true;
    }


    /**************************************************************************
     *
     * method from Activity
     *
     * called everytime the menu opens
     * Enable and disable menu items according to state and context
     *
     *************************************************************************/
    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        final int[] debugItems =
        {
            R.id.check_simulate_deleted_or_changed,
            R.id.action_scan_directories,
            R.id.action_open_db,
            R.id.action_check_db,
            R.id.action_remove_files_in_db,
            R.id.action_add_files_in_db
        };

        boolean bShowDebugMenuEntries = UserSettings.getBool(UserSettings.PREF_SHOW_DEBUG_MENU_ENTRIES, false);
        for (int item_no : debugItems)
        {
            MenuItem theMenuItem = menu.findItem(item_no);
            theMenuItem.setVisible(bShowDebugMenuEntries);
        }

        return super.onPrepareOptionsMenu(menu);
    }


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id)
        {
            case R.id.action_about:
                DialogAbout();
                return true;

            case R.id.action_settings:
                Intent intent = new Intent(this, MyPreferenceActivity.class);
                // deprecated startActivityForResult(intent, RESULT_SETTINGS);
                mPreferencesActivityLauncher.launch(intent);
                return true;

            case R.id.action_select_shared_db_path:
                dialogLocateSharedDbDirectory();
                return true;

            case R.id.action_changes:
                DialogChanges();
                break;

            case R.id.action_help:
                DialogHelp();
                break;

            case R.id.action_show_privacy_policy:
                {
                    final String url = "https://gitlab.com/AndreasK/safmediascanner/-/raw/master/privacy-policy.txt";
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                }
                break;

            case R.id.action_scan_directories:
                if (mCurrFragmentNo == 2 && mReadWritePermissionGranted)
                {
                    createScanner();
                    if (mSafScanner != null)
                    {
                        mSafScanner.runCountFiles();
                        if (mSafScanner.isBusy())
                        {
                            setBusyState(true);
                        }
                    }
                }
                break;

            case R.id.check_simulate_deleted_or_changed:
                boolean b = mMenuItemSimulate.isChecked();
                b = !b;     // invert
                mMenuItemSimulate.setChecked(b);
                return true;

            case R.id.action_open_db:
                if (mCurrFragmentNo == 2 && mReadWritePermissionGranted)
                {
                    createScanner();
                    if (mSafScanner != null)
                    {
                        mSafScanner.runOpenExistingDatabase();
                        if (mSafScanner.isBusy())
                        {
                            setBusyState(true);
                        }
                    }
                }
                break;

            case R.id.action_check_db:
                if (mCurrFragmentNo == 2 && mReadWritePermissionGranted)
                {
                    createScanner();
                    if (mSafScanner != null)
                    {
                        mSafScanner.runCheckDatabaseFiles();
                        if (mSafScanner.isBusy())
                        {
                            setBusyState(true);
                        }
                    }
                }
                break;

            case R.id.action_remove_files_in_db:
                if (mCurrFragmentNo == 2 && mReadWritePermissionGranted)
                {
                    createScanner();
                    if (mSafScanner != null)
                    {
                        mSafScanner.runRemoveDatabaseFiles();
                        if (mSafScanner.isBusy())
                        {
                            setBusyState(true);
                        }
                    }
                }
                break;

            case R.id.action_add_files_in_db:
                if (mCurrFragmentNo == 2 && mReadWritePermissionGranted)
                {
                    createScanner();
                    if (mSafScanner != null)
                    {
                        mSafScanner.runAddDatabaseFiles();
                        if (mSafScanner.isBusy())
                        {
                            setBusyState(true);
                        }
                    }
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    /**************************************************************************
     *
     * method from Activity
     *
     * This is called when a secondary activity has ended.
     * This activity is either our TagsActivity or the SAF file selector.
     *
     *************************************************************************/
    /* deprecated
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData)
    {
        Log.d(LOG_TAG, "onActivityResult(req = " + requestCode + ", res = " + resultCode + ")");

        switch (requestCode)
        {
            //
            // SAF directory selector has ended
            //

            case REQUEST_DIRECTORY_SELECT:
            case REQUEST_SHARED_DB_DIRECTORY_SELECT:
                if (resultCode == RESULT_OK)
                {
                    Uri treeUri = resultData.getData();
                    if (treeUri != null)
                    {
                        Log.d(LOG_TAG, " URI = " + treeUri.getPath());

                        grantUriPermission(getPackageName(), treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        getContentResolver().takePersistableUriPermission(treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        if (requestCode == REQUEST_SHARED_DB_DIRECTORY_SELECT)
                        {
                            mSharedDbPathUri = treeUri;
                            UserSettings.putString(PREF_SHARED_DB_DIRECTORY_URI, treeUri.toString());
                        }
                        else
                        {
                            initPermissionsList();
                            // tell fragment to redraw
                            PathTableFragment frag = getPathTableFragment();
                            if (frag != null)
                            {
                                frag.onChangedPathList(mPathArrayList, mSelectedIndex);
                            }
                        }
                    }
                }
                break;

            case RESULT_SETTINGS:
                // check for changed settings, and apply them
                break;

            default:
                super.onActivityResult(requestCode, resultCode, resultData);
                break;
        }
    }
    */


    public void onPathClicked(View v)
    {

    }




    /**************************************************************************
     *
     * helper for deprecated startActivityForResult()
     *
     *************************************************************************/
    private void registerPreferencesCallback()
    {
        mPreferencesActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
            {
                @Override
                public void onActivityResult(ActivityResult result)
                {
                }
            });
    }


    /**************************************************************************
     *
     * helper for deprecated startActivityForResult()
     *
     *************************************************************************/
    private void registerDirectorySelectCallback()
    {
        mRequestDirectorySelectActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
            {
                @Override
                public void onActivityResult(ActivityResult result)
                {
                    int resultCode = result.getResultCode();
                    Intent data = result.getData();

                    if ((resultCode == RESULT_OK) && (data != null))
                    {
                        Uri treeUri = data.getData();
                        if (treeUri != null)
                        {
                            Log.d(LOG_TAG, " URI = " + treeUri.getPath());

                            grantUriPermission(getPackageName(), treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            getContentResolver().takePersistableUriPermission(treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            initPermissionsList();
                            // tell fragment to redraw
                            PathTableFragment frag = getPathTableFragment();
                            if (frag != null)
                            {
                                frag.onChangedPathList(mPathArrayList, mSelectedIndex);
                            }
                        }
                    }
                }
            });
    }


    /**************************************************************************
     *
     * helper for deprecated startActivityForResult()
     *
     *************************************************************************/
    private void registerSharedDbDirectorySelectCallback()
    {
        mRequestSharedDbDirectorySelectActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
            {
                @Override
                public void onActivityResult(ActivityResult result)
                {
                    int resultCode = result.getResultCode();
                    Intent data = result.getData();

                    if ((resultCode == RESULT_OK) && (data != null))
                    {
                        Uri treeUri = data.getData();
                        if (treeUri != null)
                        {
                            Log.d(LOG_TAG, " URI = " + treeUri.getPath());

                            grantUriPermission(getPackageName(), treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            getContentResolver().takePersistableUriPermission(treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            mSharedDbPathUri = treeUri;
                            UserSettings.putString(PREF_SHARED_DB_DIRECTORY_URI, treeUri.toString());
                        }
                    }
                }
            });
    }


    /**************************************************************************
     *
     * called when access request has been denied or granted
     *
     *************************************************************************/
    @Override
    public void onRequestPermissionsResult
    (
        int requestCode,
        @NonNull String[] permissions,
        @NonNull int[] grantResults
    )
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(LOG_TAG, "onRequestPermissionsResult()");

        //noinspection SwitchStatementWithTooFewBranches
        switch (requestCode)
        {
            case MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE:
            {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED))
                {
                    Log.d(LOG_TAG, "onRequestPermissionsResult(): permission granted");
                    mReadWritePermissionGranted = true;
//                    setupPathList();
                } else
                {
                    Log.d(LOG_TAG, "onRequestPermissionsResult(): permission denied");
//                    Toast.makeText(getApplicationContext(), R.string.str_permission_denied, Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
            break;

            default:
                Log.d(LOG_TAG, "onRequestPermissionsResult(): unexpected request code " + requestCode);
                break;
        }
    }


    /**************************************************************************
     *
     * ask for file access permission
     *
     *************************************************************************/
    private void askPermission()
    {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED)
        {
            Log.d(LOG_TAG, "permission immediately granted");
            //   setupPathList();
            mReadWritePermissionGranted = true;
        } else
        {
            Log.d(LOG_TAG, "request permission");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE);
        }
    }


    /**************************************************************************
     *
     * helper to start SAF file selector
     *
     *************************************************************************/
    protected Intent createSafPickerIntent()
    {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        /*
        Android design flaw: only works if before opened with ACTION_OPEN_DOCUMENT
        // specify initial directory tree position
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            String sharedDir = getSharedDbBasePath();
            Uri uri = Uri.fromFile(new File(sharedDir));
            if (uri != null)
            {
                Log.d(LOG_TAG, "startSafPicker(): initial Uri = " + uri);
                intent.putExtra(EXTRA_INITIAL_URI, uri);
            }
        }
        */

        // Ask for read and write access to files and sub-directories in the user-selected directory.
        intent.addFlags(
                Intent.FLAG_GRANT_READ_URI_PERMISSION +
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION +
                        Intent.FLAG_GRANT_PREFIX_URI_PERMISSION +
                        Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
        return intent;
    }


    /************************************************************************************
     *
     * helper
     *
     ***********************************************************************************/
    private int getIndexOfOldestPersistedUriPermission(List<UriPermission> thePermissionList)
    {
        long otime = INVALID_TIME;
        int oindex = -1;

        int i = 0;
        for (UriPermission perm : thePermissionList)
        {
            long ptime = perm.getPersistedTime();
            if (ptime == INVALID_TIME)
            {
                Log.w(LOG_TAG, "getIndexOfOldestPersistedUriPermission(): invalid time for Uri " + perm.getUri());
            } else if (oindex == -1)
            {
                oindex = i;
                otime = ptime;
            } else if (ptime < otime)
            {
                otime = ptime;
                oindex = i;
            }
            i++;
        }

        return oindex;
    }


    /************************************************************************************
     *
     * strange method to get our Fragment
     *
     ***********************************************************************************/
    private PathTableFragment getPathTableFragment()
    {
        // -> https://stackoverflow.com/questions/51385067/android-navigation-architecture-component-get-current-visible-fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment myFragment = fragmentManager.findFragmentById(R.id.nav_host_fragment);
        if (myFragment != null)
        {
            Fragment fragment2 = myFragment.getChildFragmentManager().getFragments().get(0);
            if (fragment2 instanceof PathTableFragment)
            {
                return (PathTableFragment) fragment2;
            }
        }
        return null;
    }


    /************************************************************************************
     *
     * strange method to get our Fragment
     *
     ***********************************************************************************/
    private ScanFragment getScanFragment()
    {
        // -> https://stackoverflow.com/questions/51385067/android-navigation-architecture-component-get-current-visible-fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment myFragment = fragmentManager.findFragmentById(R.id.nav_host_fragment);
        if (myFragment != null)
        {
            Fragment fragment2 = myFragment.getChildFragmentManager().getFragments().get(0);
            if (fragment2 instanceof ScanFragment)
            {
                return (ScanFragment) fragment2;
            }
        }
        return null;
    }


    /************************************************************************************
     *
     * called at application start and whenever the list was changed
     * (the latter is brute force but also avoids double entries)
     *
     ***********************************************************************************/
    private void initPermissionsList()
    {
        //
        // remember selection
        //

        String selectedPath;
        if ((mSelectedIndex >= 0) && (mPathArrayList != null) && (mSelectedIndex < mPathArrayList.size()))
        {
            selectedPath = mPathArrayList.get(mSelectedIndex);
        } else
        {
            selectedPath = null;
        }

        //
        // overwrite everything with new list
        //

        mPathArrayList = new ArrayList<>();
        mSelectedIndex = -1;
        mPermissionList = getContentResolver().getPersistedUriPermissions();

        //
        // remove shared db path from permission list
        //

        String sharedDbUri = UserSettings.getString(PREF_SHARED_DB_DIRECTORY_URI);
        if (sharedDbUri != null)
        {
            mSharedDbPathUri = Uri.parse(sharedDbUri);
            for (UriPermission permission : mPermissionList)
            {
                Uri uri = permission.getUri();
                if (sharedDbUri.equals(uri.toString()))
                {
                    mPermissionList.remove(permission);
                    break;
                }
            }
        }

        // remove old entries
        while (mPermissionList.size() > maxNumOfPersistedPermissions)
        {
            int index = getIndexOfOldestPersistedUriPermission(mPermissionList);
            if (index < 0)
            {
                break;
            } else
            {
                Uri uri = mPermissionList.get(index).getUri();
                Log.d(LOG_TAG, "onCreate(): remove oldest persisted permission: " + uri);
                getContentResolver().releasePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                mPermissionList.remove(index);
            }
        }

        //
        // init path array
        //

        int i = 0;
        for (UriPermission permission : mPermissionList)
        {
            String thePath = permission.getUri().getPath();
            if (thePath != null)
            {
                mPathArrayList.add(thePath);
                if (thePath.equals(selectedPath))
                {
                    mSelectedIndex = i;
                }
                i++;
            } else
            {
                Log.e(LOG_TAG, "initPermissionsList(): null path");
            }
        }
    }


    /************************************************************************************
     *
     * get db base path from environment or from settings
     *
     * Does not work with Android 11.
     * Trying this:
     * https://developer.android.com/reference/android/provider/MediaStore.Files
     * ?
     *
     * Unclear:
     *  https://developer.android.com/training/data-storage/shared/media#direct-file-paths
     * Unclear:
     * https://developer.android.com/training/data-storage/shared/media#app-attribution
     ***********************************************************************************/
    private String getSharedDbBasePath()
    {
        File basePath;

        if (getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
        {
            basePath = getExternalStorageDirectory();
        } else
        {
            basePath = getFilesDir();
        }

        @SuppressWarnings("UnnecessaryLocalVariable") String defaultPath = basePath.getPath() + "/" + sDbPath;
        //return UserSettings.getAndPutString(UserSettings.PREF_DATA_BASE_PATH, defaultPath);
        return defaultPath;
    }


    /************************************************************************************
     *
     * helper
     *
     ***********************************************************************************/
    private void setLeftFabState(boolean bEnable)
    {
        mFabLeft.setEnabled(bEnable);

        /*
        // more complex solution:
        int id = (mNumOfSelectedFiles > 0) ? R.color.action_button_info_colour : R.color.action_button_info_disabled_colour;
        ColorStateList tint = getResources().getColorStateList(id, null);
        mFloatingButton1.setBackgroundTintList(tint);
        */
        mFabLeft.setBackgroundTintList((bEnable) ? sSavedFloatingButtonColours : ColorStateList.valueOf(0xFFE0E0E0));
    }


    /************************************************************************************
     *
     * TODO: find better solution for this
     *
     ***********************************************************************************/
    public void onChangedPathListSelection(int selectedIndex)
    {
        boolean bEnabled = (mSelectedIndex >= 0);   // current state
        boolean bEnable = (selectedIndex >= 0);     // new state

        if (bEnable != bEnabled)
        {
            setLeftFabState(bEnable);

            // tell fragment to update state
            PathTableFragment frag = getPathTableFragment();
            if (frag != null)
            {
                frag.onChangedPathListSelection(bEnable);
            }
        }

        mSelectedIndex = selectedIndex;
    }


    /************************************************************************************
     *
     * TODO: find better solution for this
     *
     ***********************************************************************************/
    private void setBusyState(boolean isBusy)
    {
        if (mCurrFragmentNo == 2)
        {
            // background scanner task is running or not
            setLeftFabState(!isBusy);
            mFabRight.setEnabled(!isBusy);
        }
    }


    /************************************************************************************
     *
     * TODO: find better solution for this
     *
     ***********************************************************************************/
    public void onChangedFragment(int n)
    {
        Log.d(LOG_TAG, "onChangedFragment(" + n + ")");
        if ((mFabLeft == null) || (mFabRight == null))
        {
            Log.w(LOG_TAG, "onChangedFragment() - invalid Activity");
            mRequestedFragmentNo = n;
            return;
        }

        if (n != mCurrFragmentNo)
        {
            if (n == 1)
            {
                //
                // path selection fragment
                //
                mFabLeft.setImageDrawable(sSavedFloatingButtonLeftDrawable);
                mFabRight.setImageDrawable(sSavedFloatingButtonRightDrawable);
                boolean bEnabled = (mSelectedIndex >= 0);   // current state
                mCurrFragmentNo = n;
                setLeftFabState(bEnabled);
            } else
            {
                //
                // scan start fragment
                //
                mFabLeft.setImageResource(R.drawable.ic_menu_play_clip);
                mFabRight.setImageResource(R.drawable.ic_search_green);
                mCurrFragmentNo = n;
                if ((mSafScanner != null) && mSafScanner.isBusy())
                {
                    setBusyState(true);
                }
                else
                {
                    mSafScanner = null;     // force creation of new object
                }
            }
        }
    }


    /**************************************************************************
     *
     * dialogue
     *
     *************************************************************************/
    private void DialogAbout()
    {
        UserSettings.AppVersionInfo info = UserSettings.getVersionInfo(this);

        final String strTitle = getString(R.string.app_name);
        final String strDescription = getString(R.string.str_app_description);
        final String strAuthor = getString(R.string.str_author);
        final String strVersion = "Version " + info.versionName + ((info.isDebug) ? " DEBUG" : "");

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(strTitle);
        alertDialog.setIcon(R.drawable.app_icon_noborder);
        alertDialog.setMessage(
                strDescription + "\n\n" +
                        strAuthor + "Andreas Kromke" + "\n\n" +
                        strVersion + "\n" +
                        "(" + info.strCreationTime + ")");
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * helper to show a html dialogue
     *
     *************************************************************************/
    private void DialogHtml(final String filename)
    {
        WebView webView = new WebView(this);
        webView.loadUrl("file:///android_asset/html-" + getString(R.string.locale_prefix) + "/" + filename);
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setView(webView);
        alertDialog.show();
    }


    /**************************************************************************
     *
     * helper to show a html dialogue
     *
     *************************************************************************/
    private void DialogHelp()
    {
        DialogHtml("help.html");
    }


    /**************************************************************************
     *
     * helper to show a html dialogue
     *
     *************************************************************************/
    private void DialogChanges()
    {
        DialogHtml("changes.html");
    }


    /**************************************************************************
     *
     * ask if file shall be overwritten
     *
     *************************************************************************/
    private void dialogAskOverwriteFile(final File oldFile, final File newFile)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_question_replace_file));
        builder.setMessage(getString(R.string.str_message_replace_file));
        builder.setPositiveButton(getString(R.string.str_replace), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                mPendingDialog = null;       // test code
                if (newFile.delete())
                {
                    if (oldFile.renameTo(newFile))
                    {
                        Toast.makeText(getApplicationContext(), "replaced", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "failure", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                mPendingDialog = null;       // test code
                Toast.makeText(getApplicationContext(), R.string.str_keep_file, Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * ask for new database name and rename the file
     *
     *************************************************************************/
    private void dialogSaveDatabaseFile(String suggestedFilename, final File dbFile)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_save_dlg_title));
        builder.setMessage(R.string.str_save_dlg_message);

        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null)
        {
            @SuppressLint("InflateParams") final View view = inflater.inflate(R.layout.dlg_save_file, null);
            final EditText textItem = view.findViewById(R.id.edit_name);
            textItem.setText(suggestedFilename);
            builder.setView(view);

            builder.setPositiveButton(getString(R.string.str_ok), new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface arg0, int arg1)
                {
                    Log.d(LOG_TAG, "dialogSaveDatabaseFile() : OK");
                    mPendingDialog = null;       // test code
                    //final EditText textItem = view.findViewById(R.id.edit_name);
                    String name = textItem.getText().toString();
                    if (!name.isEmpty())
                    {
                        if (!name.endsWith(".db"))
                        {
                            name += ".db";
                        }
                        Log.d(LOG_TAG, "dialogSaveDatabaseFile() : save file " + name);
                        File newFile = new File(dbFile.getParent(), name);
                        if (newFile.exists())
                        {
                            // name already exists. Ask for overwrite confirmation
                            dialogAskOverwriteFile(dbFile, newFile);
                        }
                        else
                        {
                            boolean bDone = dbFile.renameTo(newFile);
                            if (!bDone)
                            {
                                Toast.makeText(getApplicationContext(), "failure", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            });

            builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    mPendingDialog = null;       // test code
                    Log.d(LOG_TAG, "dialogSaveDatabaseFile() : Cancel");
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            mPendingDialog = alertDialog;       // test code
        }
    }


    /**************************************************************************
     *
     * ask for the shared db directory
     *
     *************************************************************************/
    private void dialogLocateSharedDbDirectory()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.str_select_shared_db_path);
        builder.setMessage(getString(R.string.str_navigate_to) + ": " + getSharedDbBasePath());
        builder.setPositiveButton(R.string.str_proceed, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                mPendingDialog = null;       // test code
                Intent intent = createSafPickerIntent();
                mRequestSharedDbDirectorySelectActivityLauncher.launch(intent);
            }
        });

        builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Toast.makeText(getApplicationContext(),
                        getString(R.string.str_no_shared_dir), Toast.LENGTH_LONG).show();
                mPendingDialog = null;       // test code
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /************************************************************************************
     *
     * create the scanner object
     *
     ***********************************************************************************/
    void createScanner()
    {
        if (mSafScanner == null)
        {
            // not yet created, or we need a new one

            Uri uri = getSelectedUri();
            DocumentFile df = (uri != null) ? DocumentFile.fromTreeUri(getApplicationContext(), uri) : null;
            if (df != null)
            {
                DocumentFile sdf = (mSharedDbPathUri != null) ? DocumentFile.fromTreeUri(getApplicationContext(), mSharedDbPathUri) : null;
                String suggestedDbFname = (sdf != null) ? (suggestFilenameFromUri(df.getUri()) + ".db") : sDbName;

                //Log.d(LOG_TAG, "createScanner() : getFilesDir() -> " + getFilesDir().getPath());
                //Log.d(LOG_TAG, "createScanner() : getDatabasePath() -> " + getDatabasePath(sDbName));
                String privateDbPath = getDatabasePath(sDbName).getParent();    //getFilesDir().getPath(),
                Log.d(LOG_TAG, "createScanner() : privateDbPath = " + privateDbPath);

                //
                // For whatever reason the Nexus 5 (Android 6.0.1) needs to create the database directory.
                //

                if (privateDbPath != null)
                {
                    File theDir = new File(privateDbPath);
                    if (!theDir.exists())
                    {
                        Log.w(LOG_TAG, "createScanner() : make directory " + theDir);
                        if (!theDir.mkdirs())
                        {
                            Log.e(LOG_TAG, "createScanner() : cannot make directory " + theDir);
                        }
                    }
                }

                mSafScanner = new SafScanner(
                        getApplicationContext(),
                        sdf,
                        suggestedDbFname,
                        privateDbPath,
                        df);
            }
            else
            {
                Log.e(LOG_TAG, "runScanner() : no URI?");
            }
        }
    }


    /************************************************************************************
     *
     * helper
     *
     ***********************************************************************************/
    private void runScanner(boolean bIncremental)
    {
        createScanner();
        if (mSafScanner != null)
        {
            boolean bSimulateDeletionAndChange = mMenuItemSimulate.isChecked();
            boolean bCopyDbToSafPath = UserSettings.getBool(UserSettings.PREF_COPY_DB_TO_SAF_PATH, true);
            boolean bExtractFolderIcons = UserSettings.getBool(UserSettings.PREF_EXTRACT_FOLDER_IMAGES, true);
            boolean bScaleExistingImages = UserSettings.getBool(UserSettings.PREF_SCALE_EXISTING_FOLDER_IMAGES, false);
            boolean bKeepBackup = UserSettings.getBool(UserSettings.PREF_KEEP_BACKUP_OF_ORIGINAL_FOLDER_IMAGES, false);
            boolean bRenameExistingImages = UserSettings.getBool(UserSettings.PREF_RENAME_EXISTING_FOLDER_IMAGES, false);
            boolean bUseNativeTaglib = UserSettings.getBool(UserSettings.PREF_USE_NATIVE_TAGLIB, false);
            boolean bVerifyTagReader = UserSettings.getBool(UserSettings.PREF_VERIFY_TAG_READER, false);

            int maxFolderImageSize = UserSettings.getIntStoredAsString(UserSettings.PREF_MAX_SIZE_OF_FOLDER_IMAGES, defaultFolderImageSize);
            if (maxFolderImageSize < minimumFolderImageSize)
            {
                UserSettings.putVal(UserSettings.PREF_MAX_SIZE_OF_FOLDER_IMAGES, minimumFolderImageSize);
                maxFolderImageSize = minimumFolderImageSize;
            }

            mSafScanner.applySettings(
                    bCopyDbToSafPath,
                    bExtractFolderIcons,
                    bScaleExistingImages,
                    bKeepBackup,
                    bRenameExistingImages,
                    bUseNativeTaglib,
                    bVerifyTagReader,
                    maxFolderImageSize);

            if (bIncremental)
            {
                mSafScanner.runIncrementalScan(bSimulateDeletionAndChange);
            }
            else
            {
                mSafScanner.runTotalScan(bSimulateDeletionAndChange);
            }

            if (mSafScanner.isBusy())
            {
                setBusyState(true);
            }
        }
    }


    /************************************************************************************
     *
     * helper
     *
     ***********************************************************************************/
    String suggestFilenameFromUri(Uri uri)
    {
        List<String> segments = uri.getPathSegments();
        int n = segments.size();

        String ret = "mysaf.db";

        if (n > 0)
        {
            ret = segments.get(n - 1);
        }
        return ret.replace(':', ' ').replace('/', '_');
    }


    /************************************************************************************
     *
     * TODO: find better solution for this
     *
     ***********************************************************************************/
    void tellScanResult(int res, DocumentFile df, File databaseFile)
    {
        if (res == 0)
        {
            Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
        }
        else
        if (res < 0)
        {
            Toast.makeText(getApplicationContext(), "FAILURE", Toast.LENGTH_SHORT).show();
        }
        else
        if (res == 1)
        {
            dialogSaveDatabaseFile(suggestFilenameFromUri(df.getUri()) + ".db", databaseFile);
        }

        setBusyState(false);
    }


    /************************************************************************************
     *
     * TODO: find better solution for this
     *
     ***********************************************************************************/
    Uri getSelectedUri()
    {
        if (mSelectedIndex >= 0 && mSelectedIndex < mPermissionList.size())
        {
            return mPermissionList.get(mSelectedIndex).getUri();
        }
        else
        {
            return null;
        }
    }


    public void TimerCallback()
    {
        if (mSafScanner != null)
        {
            ScanFragment frag = getScanFragment();
            if ((frag != null) && (mSafScanner.mbProgressTextChanged))
            {
                mSafScanner.mbProgressTextChanged = false;
                frag.setScanText(mSafScanner.mProgressText);
            }
        }
    }



    class MyTimerTask extends TimerTask
    {
        @Override
        public void run()
        {
            runOnUiThread(MainActivity.this::TimerCallback);
        }
    }
}
