/*
 * Copyright (C) 2018-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.safmediascanner;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import androidx.preference.PreferenceManager;

import android.os.Build;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * helper class for user settings (preferences)
 */
@SuppressWarnings("WeakerAccess")
public class UserSettings
{
    private static final String LOG_TAG = "UserSettings";

    public static final String PREF_COPY_DB_TO_SAF_PATH = "prefCopyDbToSafPath";
    public static final String PREF_EXTRACT_FOLDER_IMAGES = "prefExtractFolderImages";
    public static final String PREF_MAX_SIZE_OF_FOLDER_IMAGES = "prefMaxSizeOfFolderImages";
    public static final String PREF_SCALE_EXISTING_FOLDER_IMAGES = "prefScaleExistingFolderImages";
    public static final String PREF_KEEP_BACKUP_OF_ORIGINAL_FOLDER_IMAGES = "prefKeepBackupOfOriginalFolderImages";
    public static final String PREF_SHOW_DEBUG_MENU_ENTRIES = "prefShowDebugMenuEntries";
    public static final String PREF_USE_NATIVE_TAGLIB = "prefUseNativeTaglib";
    public static final String PREF_VERIFY_TAG_READER = "prefVerifyTagReader";
    public static final String PREF_RENAME_EXISTING_FOLDER_IMAGES = "prefRenameExistingFolderImages";
    public static final String PREF_SHARED_DB_DIRECTORY_URI = "prefSharedDbDirectoryUri";

    private static SharedPreferences mSharedPrefs;

    @SuppressWarnings("WeakerAccess")
    public static class AppVersionInfo
    {
        String versionName = "";
        int versionCode = 0;
        String strCreationTime = "";
        boolean isDebug;
    }

    @SuppressWarnings("WeakerAccess")
    public static AppVersionInfo getVersionInfo(Context context)
    {
        AppVersionInfo ret = new AppVersionInfo();
        PackageInfo packageinfo = null;

        try
        {
            packageinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        }
        catch (PackageManager.NameNotFoundException e)
        {
            Log.d(LOG_TAG, "getVersionInfo() : " + e);
        }

        if (packageinfo != null)
        {
            ret.versionName = packageinfo.versionName;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
            {
                ret.versionCode = (int) packageinfo.getLongVersionCode();
            }
            else
            {
                // deprecated in API 29
                ret.versionCode = packageinfo.versionCode;
            }
        }

        // get ISO8601 date instead of dumb US format (Z = time zone) ...
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
        Date buildDate = new Date(BuildConfig.TIMESTAMP);
        ret.strCreationTime = df.format(buildDate);
        ret.isDebug = BuildConfig.DEBUG;

        return ret;
    }


    public static void setContext(Context context)
    {
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }


    /*
    * updates a preference value and returns previous value or defVal, if none
     */
    /*
    @SuppressLint("ApplySharedPref")
    static int updateValStoredAsString(final String key, int newVal, int defVal)
    {
        int prevVal = defVal;

        if (mSharedPrefs.contains(key))
        {
            prevVal = getIntStoredAsString(key, defVal);
        }

        if (prevVal != newVal)
        {
            String vds = Integer.toString(newVal);
            SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
            prefEditor.putString(key, vds);
            prefEditor.commit();
        }

        return prevVal;
    }
    */

    /*
    * get a numerical value from the preferences and repair it, if necessary
     */
    public static int getIntStoredAsString(final String key, int defaultVal)
    {
        String vds = Integer.toString(defaultVal);
        int v;
        try
        {
            String vs = mSharedPrefs.getString(key, vds);
            if (vs == null)
            {
                vs = "invalid";     // force integer format exception in the next step
            }
            v = Integer.parseInt(vs);
        }
        catch(NumberFormatException e)
        {
            v = defaultVal;
            SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
            prefEditor.putString(key, vds);
            prefEditor.apply();
        }

        return v;
    }

    /*
    * put and commit
     */
    public static void putVal(final String key, int theVal)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.putInt(key, theVal);
        prefEditor.apply();
        prefEditor.commit();    // make sure datum is written to flash now
    }

    /*
    * put and commit
     */
    /*
    public static void putVal(final String key, boolean theVal)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.putBoolean(key, theVal);
        prefEditor.apply();
        prefEditor.commit();    // make sure datum is written to flash now
    }
    */

    /*
    * get an integer value from the preferences
     */
    /*
    public static int getInt(final String key, int defaultVal)
    {
        return mSharedPrefs.getInt(key, defaultVal);
    }
    */

    /*
    * get a boolean value from the preferences
     */
    public static boolean getBool(final String key, boolean defaultVal)
    {
        return mSharedPrefs.getBoolean(key, defaultVal);
    }

    /*
     * get a text value from the preferences
     */
    public static String getString(final String key)
    {
        return mSharedPrefs.getString(key, null);
    }

    /*
     * put a text value to the preferences
     */
    @SuppressWarnings("WeakerAccess")
    public static void putString(final String key, String value)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.putString(key, value);
        prefEditor.apply();
    }

    /*
    * get a text value from the preferences and repair it, if necessary
     */
    /*
    public static String getAndPutString(final String key, String defaultVal)
    {
        if (!mSharedPrefs.contains(key))
        {
            // not set, yet: write it
            SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
            prefEditor.putString(key, defaultVal);
            prefEditor.apply();
            return defaultVal;
        }
        return mSharedPrefs.getString(key, defaultVal);
    }
    */


    /*
     * remove, but not commit
     */
    /*
    public static void removeVal(final String key)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.remove(key);
        prefEditor.apply();
    }
    */
}
