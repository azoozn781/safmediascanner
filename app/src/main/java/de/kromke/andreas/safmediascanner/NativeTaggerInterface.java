/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.safmediascanner;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.IOException;

import androidx.annotation.Keep;
import androidx.documentfile.provider.DocumentFile;
import de.kromke.andreas.audiotags.TagsBase;

public class NativeTaggerInterface extends TaggerInterface
{
    private static final String LOG_TAG = "SMS : NativeTagger";

    // keep in sync with native code!
    @Keep private final static int cmdJniGetTagsFromPath = 1;
    @Keep private final static int cmdJniGetTagsFromFd = 2;
    @Keep private final static int cmdJniExtractPictureFromFdToFd = 3;

    // these fields are supposed to be written to from JNI
    @Keep private String jniAlbum;
    @Keep private String jniGrouping;
    @Keep private String jniTitle;
    @Keep private String jniSubtitle;
    @Keep private String jniComposer;
    @Keep private String jniPerformer;
    @Keep private String jniAlbumArtist;
    @Keep private String jniConductor;
    @Keep private String jniGenre;
    @Keep private int jniYear;
    @Keep private int jniDiskNo;             // 0: not set
    @Keep private int jniTrackNo;            // 0: not set
    @Keep private int jniTotalDiskNo;        // 0: not set (currently these are extracted, but not processed further)
    @Keep private int jniTotalTrackNo;       // 0: not set
    @Keep private int jniDuration;           // 0: not set
    @Keep private int jniTagType;            // 0: unknown, 1: Id3v1, 2: Id3v2, 10: Mp4, 11: Ogg
    @Keep private int jniPicType;            // 0: none, 1: jpeg, 2: png
    @Keep private int jniPicSize;

    static
    {
        System.loadLibrary("native-lib");
    }
    public native int getTagsJNI(final String filepath, long length, long lastModified, int command, int param1, int param2);

    private static String doTrim(String v)
    {
        // IDE thinks that v is always null, but v is a JNI variable.
        //noinspection ConstantConditions
        return (v == null) ? v : v.trim();
    }

    private MusicDatabase.AudioFile _getTags(int fd, final Uri uri, long length, long lastModified)
    {
        int rc = getTagsJNI(uri.getPath(), length, lastModified, cmdJniGetTagsFromFd, fd, 0);
        Log.d(LOG_TAG, "getTagsJNI() : got result " + rc + " from JNI");

        if (rc == 0)
        {
            MusicDatabase.AudioFile af = new MusicDatabase.AudioFile();
            af.id           = -1;               // add later
            af.track_no     = 1000 * jniDiskNo + jniTrackNo;

            af.title        = doTrim(jniTitle);
            af.album        = doTrim(jniAlbum);
            af.album_id     = -1;               // add later
            af.duration     = jniDuration;
            af.grouping     = doTrim(jniGrouping);
            af.subtitle     = doTrim(jniSubtitle);
            af.composer     = doTrim(jniComposer);
            af.performer    = doTrim(jniPerformer);
            af.album_artist = doTrim(jniAlbumArtist);
            af.conductor    = doTrim(jniConductor);
            af.genre        = doTrim(jniGenre);
            af.year         = jniYear;
            af.uri          = uri.toString();
            af.pic_type     = jniPicType;       // 0: none, 1: jpeg, 2: png
            af.pic_size     = jniPicSize;
            af.tag_type     = jniTagType;
            af.mtime        = lastModified;

            // HACK: repair genre
            String genre = TagsBase.numericalGenreToString(af.genre);
            if (genre != null)
            {
                Log.d(LOG_TAG, "NativeTaggerInterface._getTags() -- genre " + af.genre + " = replaced by " + genre);
                af.genre = genre;
            }

            return af;
        }
        return null;
    }


    /**************************************************************************
     *
     * get metadata from audio file
     *
     *************************************************************************/
    public MusicDatabase.AudioFile getTags(final DocumentFile df, ContentResolver resolver)
    {
        ParcelFileDescriptor filePfd;
        MusicDatabase.AudioFile af = null;

        try
        {
            filePfd = resolver.openFileDescriptor(df.getUri(), "r");
            if (filePfd == null)
            {
                Log.e(LOG_TAG, "handleAudioFile() : cannot open: \"" + df.getUri().getPath() + "\"");
            } else
            {
                int fd = filePfd.getFd();
                Log.d(LOG_TAG, "handleAudioFile() : got fd " + fd);
                /*
                String fdpath = "/proc/" + mProcessId + "/fd/" + fd;
                Log.d(LOG_TAG, "handleAudioFile() : trying to access \"" + fdpath + "\"");
                File f = new File(fdpath);
                if (f.exists())
                {
                    Log.d(LOG_TAG, "handleAudioFile() : file exists");
                }
                if (f.canRead())
                {
                    Log.d(LOG_TAG, "handleAudioFile() : can read");
                }
                if (f.canWrite())
                {
                    Log.d(LOG_TAG, "handleAudioFile() : can write");
                }

                    //JNI:
                    //    FILE *file = fdopen(fd, "rb");

                int rc = getTagsJNI(fdpath, 1, 0);
                Log.d(LOG_TAG, "handleAudioFile() : got result " + rc + " from JNI");
                 */

                af = _getTags(fd, df.getUri(), df.length(), df.lastModified());

                try
                {
                    filePfd.close();
                } catch (IOException e)
                {
                    Log.e(LOG_TAG, "handleAudioFile() : cannot close: \"" + df.getUri().getPath() + "\"");
                }
            }
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "handleAudioFile() : file not found: \"" + df.getUri().getPath() + "\"");
            return null;
        }

        return af;
    }

    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    @SuppressWarnings("SameParameterValue")
    private ParcelFileDescriptor openDf(final DocumentFile df, ContentResolver resolver, final String mode)
    {
        ParcelFileDescriptor pfd;

        try
        {
            pfd = resolver.openFileDescriptor(df.getUri(), mode);
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "extractPicture() : cannot open: \"" + df.getUri().getPath() + "\": " + e);
            return null;
        }

        return pfd;
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private void closeDf(ParcelFileDescriptor pfd)
    {
        if (pfd != null)
        {
            try
            {
                pfd.close();
            } catch (IOException e)
            {
                Log.e(LOG_TAG, "closeDf() : cannot close: " + e);
            }
        }
    }


    /**************************************************************************
     *
     * get FDs from PFDs from DocumentFiles and then call native code
     *
     *************************************************************************/
    int extractPicture
    (
        final DocumentFile dfAudioFile,
        final ContentResolver resolver,
        final String path,
        long length,
        long lastModified,
        final DocumentFile dfPicture
    )
    {
        ParcelFileDescriptor pfdPicture = openDf(dfPicture, resolver,"rw");
        if (pfdPicture == null)
        {
            Log.e(LOG_TAG, "extractPicture() : cannot open created picture file \"" + dfPicture.getUri().getPath() + "\"");
            return -1;
        }
        int fdPicture = pfdPicture.getFd();

        ParcelFileDescriptor pfdAudioFile = openDf(dfAudioFile, resolver, "r");
        if (pfdAudioFile == null)
        {
            Log.e(LOG_TAG, "extractPicture() : cannot open: \"" + dfAudioFile.getUri().getPath() + "\"");
            closeDf(pfdPicture);
            return -1;
        }
        int fdAudioFile = pfdAudioFile.getFd();

        Log.d(LOG_TAG, "extractPicture() : got fd " + fdAudioFile + " and " + fdPicture);
        int rc = getTagsJNI(path, length, lastModified, cmdJniExtractPictureFromFdToFd, fdAudioFile, fdPicture);

        closeDf(pfdPicture);
        closeDf(pfdAudioFile);
        return rc;
    }
}
