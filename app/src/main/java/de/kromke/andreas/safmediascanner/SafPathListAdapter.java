/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.safmediascanner;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SafPathListAdapter extends RecyclerView.Adapter<SafPathListAdapter.MyViewHolder>
{
    private static final String LOG_TAG = "SMS : SafPathListAdap";
    private final ArrayList<String> mDataset;
    private int mSelected;
    private final Context mContext;

    /**************************************************************************
     *
     * object representing a given row
     *
     * Note that this class cannot be static as we must access mDataset
     *
     *************************************************************************/
    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
    {
        // each data item is just a string in this case
        TextView mTextView;

        //
        // constructor, common data for each row, not individual, yet
        //
        MyViewHolder(View v)
        {
            super(v);          // this sets the basic class attribute itemView := v
            mTextView = itemView.findViewById(R.id.path_text);
            itemView.setOnLongClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view)
        {
            int position = getPositionFromView(view);
            Log.d(LOG_TAG, "onClick(pos = " + position + ")");
            if (position == mSelected)
            {
                // deselect
                mSelected = -1;
                notifyItemChanged(position);
            }
            else
            {
                if (mSelected >= 0)
                {
                    notifyItemChanged(mSelected);
                }
                mSelected = position;
                notifyItemChanged(position);
            }

            // TODO: find better solution
            if (mContext instanceof MainActivity)
            {
                ((MainActivity) mContext).onChangedPathListSelection(mSelected);
            }
            /*
            if (mOnLongClickListener != null)
            {
                mOnLongClickListener.onLongClick(view);
            }
            */
        }

        @Override
        public boolean onLongClick(View view)
        {
            int position = getPositionFromView(view);
            Log.d(LOG_TAG, "onLongClick(pos = " + position + ")");
            /*
            if (mOnLongClickListener != null)
            {
                mOnLongClickListener.onLongClick(view);
            }
            */
            return true;
        }

        //
        // individualise the object for a given row
        //
        void setData(int position)
        {
            mTextView.setText(mDataset.get(position));
            itemView.setTag(position);
            if (position == mSelected)
            {
                //Log.d(LOG_TAG, "setData(pos = " + position + ") : draw selected");
                itemView.setBackgroundColor(0x300000ff);        // semi transparent blue
                //mTextView.setBackgroundColor(0xffE8EAF6);
                mTextView.setTextColor(0xff000000);
            }
            else
            {
                //Log.d(LOG_TAG, "setData(pos = " + position + ") : draw unselected");
                itemView.setBackgroundColor(0x00000000);        // transparent
                //mTextView.setBackgroundColor(0xffFFFFFF);
                mTextView.setTextColor(0xff606060);
            }
        }
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private int getPositionFromView(View view)
    {
        Object theTag = view.getTag();
        if (theTag == null)
        {
            Log.e(LOG_TAG, "onTrackClicked() : no tag");
            return -1;
        }
        String theTagString = theTag.toString();
        int position;
        try
        {
            position = Integer.parseInt(theTagString);
        }
        catch(NumberFormatException e)
        {
            position = 0;
        }
        return position;
    }


    /**************************************************************************
     *
     * constructor
     *
     *************************************************************************/
    SafPathListAdapter(ArrayList<String> myDataset, int selectedIndex, Context context)
    {
        mDataset = myDataset;
        mContext = context;
        mSelected = selectedIndex;
    }


    /**************************************************************************
     *
     * create a "view holder" which contains our layout and custom meta information
     *
     * Note that this call does not yet handle a special row, but only handles
     * information that is common to all rows.
     *
     *************************************************************************/
    @NonNull
    @Override
    public SafPathListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                     int viewType)
    {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.path_entry, parent, false);
        return new MyViewHolder(v);
    }


    /**************************************************************************
     *
     * bind a "view holder" with data for a given row
     *
     * This takes a common object and individualises it for a given row.
     *
     *************************************************************************/
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        holder.setData(position);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
