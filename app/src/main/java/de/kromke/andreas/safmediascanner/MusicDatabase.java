/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.safmediascanner;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import androidx.documentfile.provider.DocumentFile;


public class MusicDatabase
{
    private static final String LOG_TAG = "SMS : MusicDatabase";
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 2;
    //private Context mContext;

    private SQLiteDatabase mDb;

    /* this block has to be kept in sync with corresponding definitions in Music Scanner */
    private final static String FILES_TABLE_NAME = "MUSICFILES";
    private final static String ALBUMS_TABLE_NAME = "ALBUMS";
    private final static String INFO_TABLE_NAME = "INFORMATION";

    private final static String ALBUMS_TABLE_TCOL_ID         = "_id";
    private final static String ALBUMS_TABLE_TCOL_NAME       = "album";
    private final static String ALBUMS_TABLE_TCOL_ARTIST     = "artist";
    private final static String ALBUMS_TABLE_TCOL_COMPOSER   = "composer";
    private final static String ALBUMS_TABLE_TCOL_PERFORMER  = "performer";
    private final static String ALBUMS_TABLE_TCOL_CONDUCTOR  = "conductor";
    private final static String ALBUMS_TABLE_TCOL_GENRE      = "genre";
    private final static String ALBUMS_TABLE_TCOL_FIRST_YEAR = "minyear";
    private final static String ALBUMS_TABLE_TCOL_LAST_YEAR  = "maxyear";
    private final static String ALBUMS_TABLE_TCOL_NO_TRACKS  = "numsongs";
    private final static String ALBUMS_TABLE_TCOL_DURATION   = "duration";
    private final static String ALBUMS_TABLE_TCOL_PATH       = "path";          // in fact an URI string
    private final static String ALBUMS_TABLE_TCOL_PICTURE    = "album_art";     // in fact an URI string

    private final static String FILES_TABLE_TCOL_ID           = "_id";
    private final static String FILES_TABLE_TCOL_TRACK_NO     = "track";
    private final static String FILES_TABLE_TCOL_TITLE        = "title";
    private final static String FILES_TABLE_TCOL_ALBUM        = "album";
    private final static String FILES_TABLE_TCOL_ALBUM_ID     = "album_id";
    private final static String FILES_TABLE_TCOL_DURATION     = "duration";
    private final static String FILES_TABLE_TCOL_GROUPING     = "grouping";
    private final static String FILES_TABLE_TCOL_SUBTITLE     = "subtitle";
    private final static String FILES_TABLE_TCOL_COMPOSER     = "composer";
    private final static String FILES_TABLE_TCOL_PERFORMER    = "artist";
    private final static String FILES_TABLE_TCOL_ALBUM_ARTIST = "album_artist";
    private final static String FILES_TABLE_TCOL_CONDUCTOR    = "conductor";
    private final static String FILES_TABLE_TCOL_GENRE        = "genre";
    private final static String FILES_TABLE_TCOL_YEAR         = "year";
    private final static String FILES_TABLE_TCOL_PATH         = "path";         // in fact an URI string
    private final static String FILES_TABLE_TCOL_PIC_TYPE     = "pic_type";     // 0: none, 1: jpeg, 2: png
    private final static String FILES_TABLE_TCOL_PIC_SIZE     = "pic_size";
    private final static String FILES_TABLE_TCOL_TAG_TYPE     = "tag_type";     // tagType
    private final static String FILES_TABLE_TCOL_MTIME        = "mtime";        // modification time [s]

    private static final String SQL_CREATE_INFO_TABLE =
            "CREATE TABLE IF NOT EXISTS " + INFO_TABLE_NAME + "("  +
                "DB_VERSION" + " INTEGER);";

    private static final String SQL_WRITE_INFO_TABLE =
            "INSERT INTO " + INFO_TABLE_NAME + " (DB_VERSION) VALUES (" + DATABASE_VERSION + ");";

    private static final String SQL_CREATE_FILES_TABLE =
            "CREATE TABLE IF NOT EXISTS " + FILES_TABLE_NAME + " (" +
                FILES_TABLE_TCOL_ID           + " INTEGER PRIMARY KEY NOT NULL," +
                FILES_TABLE_TCOL_TRACK_NO     + " INTEGER," +
                FILES_TABLE_TCOL_TITLE        + " TEXT," +
                FILES_TABLE_TCOL_ALBUM        + " TEXT," +
                FILES_TABLE_TCOL_ALBUM_ID     + " INTEGER," +
                FILES_TABLE_TCOL_DURATION     + " INTEGER," +
                FILES_TABLE_TCOL_GROUPING     + " TEXT," +
                FILES_TABLE_TCOL_SUBTITLE     + " TEXT," +
                FILES_TABLE_TCOL_COMPOSER     + " TEXT," +
                FILES_TABLE_TCOL_PERFORMER    + " TEXT," +
                FILES_TABLE_TCOL_ALBUM_ARTIST + " TEXT," +
                FILES_TABLE_TCOL_CONDUCTOR    + " TEXT," +
                FILES_TABLE_TCOL_GENRE        + " TEXT," +
                FILES_TABLE_TCOL_YEAR         + " INTEGER," +
                FILES_TABLE_TCOL_PATH         + " TEXT NOT NULL UNIQUE ON CONFLICT ROLLBACK," +
                FILES_TABLE_TCOL_PIC_TYPE     + " INTEGER," +
                FILES_TABLE_TCOL_PIC_SIZE     + " INTEGER," +
                FILES_TABLE_TCOL_TAG_TYPE     + " INTEGER," +
                FILES_TABLE_TCOL_MTIME        + " INTEGER" +
            ");";

    private static final String SQL_CREATE_ALBUMS_TABLE =
            "CREATE TABLE IF NOT EXISTS " + ALBUMS_TABLE_NAME + "(" +
                ALBUMS_TABLE_TCOL_ID         + " INTEGER PRIMARY KEY NOT NULL," +
                ALBUMS_TABLE_TCOL_NAME       + " TEXT," +
                ALBUMS_TABLE_TCOL_ARTIST     + " TEXT," +
                ALBUMS_TABLE_TCOL_COMPOSER   + " TEXT," +
                ALBUMS_TABLE_TCOL_PERFORMER  + " TEXT," +
                ALBUMS_TABLE_TCOL_CONDUCTOR  + " TEXT," +
                ALBUMS_TABLE_TCOL_GENRE      + " TEXT," +
                ALBUMS_TABLE_TCOL_FIRST_YEAR + " INTEGER," +
                ALBUMS_TABLE_TCOL_LAST_YEAR  + " INTEGER," +
                ALBUMS_TABLE_TCOL_NO_TRACKS  + " INTEGER," +
                ALBUMS_TABLE_TCOL_DURATION   + " INTEGER," +
                ALBUMS_TABLE_TCOL_PATH       + " TEXT," +
                ALBUMS_TABLE_TCOL_PICTURE    + " TEXT" +
            ");";

    private static final String SQL_DELETE_INFO_TABLE =
            "DROP TABLE IF EXISTS " + INFO_TABLE_NAME;

    private static final String SQL_DELETE_FILES_TABLE =
            "DROP TABLE IF EXISTS " + FILES_TABLE_NAME;

    private static final String SQL_DELETE_ALBUMS_TABLE =
            "DROP TABLE IF EXISTS " + ALBUMS_TABLE_NAME;

    // these two lists are populated by runSqlQueryCheckAllFiles()
    private ArrayList<AudioFileMinimumInfo> mAudioFilesRemoved = null;
    private ArrayList<AudioFileMinimumInfo> mAudioFilesChanged = null;
    private long mNextAudioFileId = -1;
    private long mNextAudioAlbumId = -1;

    public static class FoundAudioFile
    {
        long mtime;
        DocumentFile df;
    }

    public static class AudioAlbum
    {
        long   id = 0;
        String name = null;
        String artist = null;
        String composer = null;
        String performer = null;
        String conductor = null;
        String genre = null;
        int    first_year = 0;
        int    last_year = 0;
        int    no_tracks = 0;
        int    duration = 0;
        String uri = null;
        String pic_uri = null;
        int no_disks = 0;               // not written do db
    }

    static class AudioFile
    {
        int    id;
        int    track_no;
        String title;
        String album;
        int    album_id;
        int    duration;
        String grouping;
        String subtitle;
        String composer;
        String performer;
        String album_artist;
        String conductor;
        String genre;
        int    year;
        String uri;
        int    pic_type;
        int    pic_size;
        int    tag_type;
        long   mtime;
    }

    static class AudioFileMinimumInfo
    {
        int    id;
        String album;
        int    album_id;
        String album_artist;
    }


    /**************************************************************************
     *
     * the constructor needs the db path to access the db
     *
     *************************************************************************/
    MusicDatabase(/*Context context,*/ final String dbPath, final String dbName, boolean bCreate)
    {
        File f = new File(dbPath, dbName);
        //mContext = context;
        String absPath = f.getPath();
        int flags = SQLiteDatabase.OPEN_READWRITE;
        if (bCreate)
        {
            flags += SQLiteDatabase.CREATE_IF_NECESSARY;
        }

        try
        {
            Log.v(LOG_TAG, "open (create=" + bCreate + ") database \"" + absPath + "\"");
            mDb = SQLiteDatabase.openDatabase(absPath, null, flags);
        }
        catch(Exception e)
        {
            Log.e(LOG_TAG, "cannot open database \"" + absPath + "\"");
            mDb = null;
            return;
        }

        if (bCreate)
        {
            mDb.setVersion(DATABASE_VERSION);
        }
        else
        if (mDb.needUpgrade(DATABASE_VERSION))
        {
            Log.w(LOG_TAG, "DB: needs upgrade");
            deleteAllTables();
        }
    }


    /**************************************************************************
     *
     * check if db exists
     *
     *************************************************************************/
    public boolean exists()
    {
        return mDb != null;
    }


    /**************************************************************************
     *
     * get number of files
     *
     *************************************************************************/
    public int getNumberOfFiles()
    {
        final String value = "Count(*)";
        return (int) simpleQuery(FILES_TABLE_NAME, value);
    }


    /**************************************************************************
     *
     * get number of albums
     *
     *************************************************************************/
    public int getNumberOfAlbums()
    {
        final String value = "Count(*)";
        return (int) simpleQuery(ALBUMS_TABLE_NAME, value);
    }


    /* *****************************************************************************
     *
     * generic simple query
     *
     *****************************************************************************/
    private long simpleQuery(final String table, final String value)
    {
        final String[] COLUMNS = {value};

        try
        {
            Cursor theCursor = mDb.query(
                    table,          // The table to query
                    COLUMNS,                     // The columns to return
                    null,               // The columns for the WHERE clause
                    null,            // The values for the WHERE clause
                    null,                // don't group the rows
                    null,                 // don't filter by row groups
                    null                 // The sort order
            );

            if (theCursor == null)
            {
                Log.v(LOG_TAG, "no cursor");
                return -1;
            }
            if (!theCursor.moveToFirst())
            {
                Log.v(LOG_TAG, "no row found");
                theCursor.close();
                return -1;
            }
            long retval = theCursor.getInt(0);
            theCursor.close();
            return retval;
        }
        catch (SQLiteException e)
        {
            Log.e(LOG_TAG, "SQLiteException: " + e);
            return -1;
        }
    }


    /* *****************************************************************************
     *
     * get max id of table
     *
     *****************************************************************************/
    /*
    long getMaxAlbumId()
    {
        final String[] COLUMNS = {"Max(" +  ALBUMS_TABLE_TCOL_ID + ")"};

        Cursor theCursor = mDb.query(
                ALBUMS_TABLE_NAME,          // The table to query
                COLUMNS,                     // The columns to return
                null,                             // The columns for the WHERE clause
                null,                  // The values for the WHERE clause
                null,                      // don't group the rows
                null,                       // don't filter by row groups
                null                          // The sort order
        );

        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return -1;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no row found");
            theCursor.close();
            return -1;
        }
        long retval = theCursor.getInt(0);
        theCursor.close();
        return retval;
    }
     */


    /* *****************************************************************************
     *
     * get max id of table
     *
     *****************************************************************************/
    /*
    long getMaxFileId()
    {
        final String[] COLUMNS = {"Max(" +  FILES_TABLE_TCOL_ID + ")"};

        Cursor theCursor = mDb.query(
                FILES_TABLE_NAME,          // The table to query
                COLUMNS,                     // The columns to return
                null,                             // The columns for the WHERE clause
                null,                  // The values for the WHERE clause
                null,                      // don't group the rows
                null,                       // don't filter by row groups
                null                          // The sort order
        );

        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return -1;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no row found");
            theCursor.close();
            return -1;
        }
        long retval = theCursor.getInt(0);
        theCursor.close();
        return retval;
    }
     */


    /******************************************************************************
     *
     * insert albums
     *
     *****************************************************************************/
    private void insertIntoAlbumsTable(ArrayList<AudioAlbum> theList)
    {
        for (int i = 0; i < theList.size(); i++)
        {
            AudioAlbum theAlbum = theList.get(i);

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();

            values.put(ALBUMS_TABLE_TCOL_ID        , theAlbum.id);
            values.put(ALBUMS_TABLE_TCOL_NAME      , theAlbum.name);
            values.put(ALBUMS_TABLE_TCOL_ARTIST    , theAlbum.artist);
            values.put(ALBUMS_TABLE_TCOL_COMPOSER  , theAlbum.composer);
            values.put(ALBUMS_TABLE_TCOL_PERFORMER , theAlbum.performer);
            values.put(ALBUMS_TABLE_TCOL_CONDUCTOR , theAlbum.conductor);
            values.put(ALBUMS_TABLE_TCOL_GENRE     , theAlbum.genre);
            values.put(ALBUMS_TABLE_TCOL_FIRST_YEAR, theAlbum.first_year);
            values.put(ALBUMS_TABLE_TCOL_LAST_YEAR , theAlbum.last_year);
            values.put(ALBUMS_TABLE_TCOL_NO_TRACKS , theAlbum.no_tracks);
            values.put(ALBUMS_TABLE_TCOL_DURATION  , theAlbum.duration);
            values.put(ALBUMS_TABLE_TCOL_PATH      , theAlbum.uri);
            values.put(ALBUMS_TABLE_TCOL_PICTURE   , theAlbum.pic_uri);

            // Insert the new row, returning the primary key value of the new row
            /*long newRowId;
            newRowId = */mDb.insert(
                ALBUMS_TABLE_NAME,
                null,
                values);
        }
    }


    /******************************************************************************
     *
     * insert tracks
     *
     *****************************************************************************/
    void insertIntoFilesTable(ArrayList<AudioFile> theList)
    {
        for (int i = 0; i < theList.size(); i++)
        {
            AudioFile theTrack = theList.get(i);

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();

            values.put(FILES_TABLE_TCOL_ID          , theTrack.id);
            values.put(FILES_TABLE_TCOL_TRACK_NO    , theTrack.track_no);
            values.put(FILES_TABLE_TCOL_TITLE       , theTrack.title);
            values.put(FILES_TABLE_TCOL_ALBUM       , theTrack.album);
            values.put(FILES_TABLE_TCOL_ALBUM_ID    , theTrack.album_id);
            values.put(FILES_TABLE_TCOL_DURATION    , theTrack.duration);
            values.put(FILES_TABLE_TCOL_GROUPING    , theTrack.grouping);
            values.put(FILES_TABLE_TCOL_SUBTITLE    , theTrack.subtitle);
            values.put(FILES_TABLE_TCOL_COMPOSER    , theTrack.composer);
            values.put(FILES_TABLE_TCOL_PERFORMER   , theTrack.performer);
            values.put(FILES_TABLE_TCOL_ALBUM_ARTIST, theTrack.album_artist);
            values.put(FILES_TABLE_TCOL_CONDUCTOR   , theTrack.conductor);
            values.put(FILES_TABLE_TCOL_GENRE       , theTrack.genre);
            values.put(FILES_TABLE_TCOL_YEAR        , theTrack.year);
            values.put(FILES_TABLE_TCOL_PATH        , theTrack.uri);
            values.put(FILES_TABLE_TCOL_PIC_TYPE    , theTrack.pic_type);
            values.put(FILES_TABLE_TCOL_PIC_SIZE    , theTrack.pic_size);
            values.put(FILES_TABLE_TCOL_TAG_TYPE    , theTrack.tag_type);
            values.put(FILES_TABLE_TCOL_MTIME       , theTrack.mtime);

            // Insert the new row, returning the primary key value of the new row
            /*long newRowId;
            newRowId = */mDb.insert(
                FILES_TABLE_NAME,
                null,
                values);
        }
    }


    /******************************************************************************
     *
     * insert tracks
     *
     *****************************************************************************/
    void insertIntoFilesTable(AudioFile af)
    {
        ArrayList<MusicDatabase.AudioFile> audioFiles = new ArrayList<>(1);
        audioFiles.add(af);
        insertIntoFilesTable(audioFiles);
    }


    /******************************************************************************
     *
     * helper to check if directory name is CD<n>
     *
     * returns <n> or -1 in case of no match
     *
     *****************************************************************************/
    private static int checkDirNameIsCdNumber(String dirname)
    {
        if (dirname.startsWith("CD"))
        {
            try
            {
                return Integer.decode(dirname.substring(2));
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        return -1;
    }


    /******************************************************************************
     *
     * helper
     *
     * uriStr is like:
     *
     * content://com.android.externalstorage.documents/tree/42B5-1A05%3AMusic%2FSafTest/document/42B5-1A05%3AMusic%2FSafTest%2FBach%20-%20Brandenburgische%20Konzerte%20-%20I%20Musici%20-%20FLAC%2F101-I%20Musici%20-%20Brandenburgisches%20Konzert%20Nr.%201%20F-Dur%20BWV%201046-1.%20Satz.flac
     *
     * Note that this is more or less a hack, because it assumes some internal
     * format of the Uri, for example %2F as path separator.
     *
     *****************************************************************************/
    private String getAlbumUri(String uriStr)
    {
        // split into filename and path
        int cpos = uriStr.lastIndexOf("%3A");       // make sure not to touch the first two of the three sections
        int pos = uriStr.lastIndexOf("%2F");
        if (pos > cpos)
        {
            String pathUri = uriStr.substring(0, pos);
            pos = pathUri.lastIndexOf("%2F");
            if (pos > cpos)
            {
                String dirName = pathUri.substring(pos + 3);
                if (checkDirNameIsCdNumber(dirName) >= 0)
                {
                    return pathUri.substring(0, pos);
                }
            }
            return pathUri;
        }
        return uriStr;
    }


    /******************************************************************************
     *
     * write album id to audio files table
     *
     *****************************************************************************/
    private void runSqlUpdateAlbumIdsInFilesTable
    (
        AudioAlbum album_info
    )
    {
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(FILES_TABLE_TCOL_ALBUM_ID, album_info.id);

        boolean bIsArtist = (album_info.artist != null) && !album_info.artist.isEmpty();
        String where;
        String[] whereVal;
        if (bIsArtist)
        {
            where = FILES_TABLE_TCOL_ALBUM + " = ? AND " + FILES_TABLE_TCOL_ALBUM_ARTIST + " = ?";
            whereVal = new String[]{album_info.name, album_info.artist};
        }
        else
        {
            where =  FILES_TABLE_TCOL_ALBUM + " = ? AND " + FILES_TABLE_TCOL_ALBUM_ARTIST + " IS NULL";
            whereVal = new String[]{album_info.name};
        }

        int rc = mDb.update(
                FILES_TABLE_NAME,
                values,
                where,
                whereVal);
        Log.v(LOG_TAG, "number of updated rows: " + rc);
    }


    /******************************************************************************
     *
     * remove a single audio file from table
     *
     *****************************************************************************/
    private void runSqlRemoveFromFilesTable
    (
        int id
    )
    {
        String where;
        String[] whereVal;
        where = FILES_TABLE_TCOL_ID + " = ?";
        whereVal = new String[]{"" + id};

        try
        {
            int rc = mDb.delete(
                    FILES_TABLE_NAME,
                    where,
                    whereVal);
            Log.v(LOG_TAG, "number of deleted files table rows: " + rc);
        }
        catch(Exception e)
        {
            Log.e(LOG_TAG, "cannot delete audio file " + id + " from table: " + e.getMessage());
        }

    }


    /******************************************************************************
     *
     * remove a single album from table
     *
     *****************************************************************************/
    private void runSqlRemoveFromAlbumsTable
    (
        long id
    )
    {
        String where;
        String[] whereVal;
        where = ALBUMS_TABLE_TCOL_ID + " = ?";
        whereVal = new String[]{"" + id};

        int rc = mDb.delete(
                ALBUMS_TABLE_NAME,
                where,
                whereVal);
        Log.v(LOG_TAG, "number of deleted albums table rows: " + rc);
    }


    /******************************************************************************
     *
     * build one album, given by album id
     *
     *****************************************************************************/
    private void runSqlCreateAlbumFromFiles
    (
        int album_id,
        final String album_name,
        final String album_artist,
        boolean extractFolderIcons
    )
    {
        AudioAlbum info = new AudioAlbum();
        info.name = album_name;
        info.artist = album_artist;

        runSqlQueryGetAlbumInfoFromAudioFiles(album_id, info, extractFolderIcons);
        Log.v(LOG_TAG, "DB: found " + info.no_tracks + " tracks for this album");

        if (info.no_tracks > 0)
        {
            info.id = album_id;
            info.name = album_name;
            info.artist = album_artist;
            ArrayList<AudioAlbum>albums = new ArrayList<>(1);
            albums.add(info);
            insertIntoAlbumsTable(albums);
        }
    }


    /******************************************************************************
     *
     * collect album information from file table
     *
     *****************************************************************************/
    private void runSqlQueryGetAlbumInfoFromAudioFiles
    (
        long album_id,
        AudioAlbum album_info,
        boolean extractFolderIcons
    )
    {
        boolean bIsId = (album_id != 0);
        boolean bIsArtist = (album_info.artist != null) && !album_info.artist.isEmpty();

        String where;
        String[] whereVal;
        if (bIsId)
        {
            where = FILES_TABLE_TCOL_ALBUM_ID + " = ?";
            whereVal = new String[]{Long.toString(album_id)};
        }
        else
        if (bIsArtist)
        {
            where = FILES_TABLE_TCOL_ALBUM + " = ? AND " + FILES_TABLE_TCOL_ALBUM_ARTIST + " = ?";
            whereVal = new String[]{album_info.name, album_info.artist};
        }
        else
        {
            where = FILES_TABLE_TCOL_ALBUM + " = ? AND " + FILES_TABLE_TCOL_ALBUM_ARTIST + " IS NULL";
            whereVal = new String[]{album_info.name};
        }

        Cursor theCursor = mDb.query(
                false,              // distinct
                FILES_TABLE_NAME,           // table to query
                null,              // columns to return: all
                where,                      // columns for the WHERE clause
                whereVal,                   // values for the WHERE clause, replacing the '?'
                null,              // don't group the rows
                null,               // don't filter by row groups
                null,              // The sort order
                null                  // limit
        );

        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no albums found");
            theCursor.close();
            return;
        }

        String path_embed = null;   // path of audio file with embedded picture
        int size_embed = 0;

        int composerColIndx  = theCursor.getColumnIndex(FILES_TABLE_TCOL_COMPOSER);
        int performerColIndx = theCursor.getColumnIndex(FILES_TABLE_TCOL_PERFORMER);
        int conductorColIndx = theCursor.getColumnIndex(FILES_TABLE_TCOL_CONDUCTOR);
        int genreColIndx     = theCursor.getColumnIndex(FILES_TABLE_TCOL_GENRE);
        int pathColIndx      = theCursor.getColumnIndex(FILES_TABLE_TCOL_PATH);
        int yearColIndx      = theCursor.getColumnIndex(FILES_TABLE_TCOL_YEAR);
        int durationColIndx  = theCursor.getColumnIndex(FILES_TABLE_TCOL_DURATION);
        int trackColIndx     = theCursor.getColumnIndex(FILES_TABLE_TCOL_TRACK_NO);
        int pic_sizeColIndx  = theCursor.getColumnIndex(FILES_TABLE_TCOL_PIC_SIZE);

        // album_info has already been initialised to zero
        do
        {
            String composer  = theCursor.getString(composerColIndx);
            String performer = theCursor.getString(performerColIndx);
            String conductor = theCursor.getString(conductorColIndx);
            String genre     = theCursor.getString(genreColIndx);
            String uri       = theCursor.getString(pathColIndx);
            int year         = theCursor.getInt(yearColIndx);
            int duration     = theCursor.getInt(durationColIndx);
            int track        = theCursor.getInt(trackColIndx);
            int pic_size     = theCursor.getInt(pic_sizeColIndx);

            album_info.composer  = strCpyUnique(album_info.composer, composer);
            album_info.performer = strCpyUnique(album_info.performer, performer);
            album_info.conductor = strCpyUnique(album_info.conductor, conductor);
            album_info.genre     = strCpyUnique(album_info.genre, genre);

            if (year > 0)
            {
                if ((album_info.first_year == 0) || (year < album_info.first_year))
                {
                    album_info.first_year = year;
                }

                if ((album_info.last_year == 0) || (year > album_info.last_year))
                {
                    album_info.last_year = year;
                }
            }

            album_info.duration += duration;
            album_info.no_tracks++;

            int disk = track % 1000;
            if (disk > album_info.no_disks)
            {
                album_info.no_disks = disk;
            }

            if (album_info.pic_uri == null)
            {
                // no album picture, yet

                String albumPath = getAlbumUri(uri);
                if (!albumPath.equals(album_info.uri))
                {
                    // first album path, or it differs from previous one
                    if (album_info.uri == null)
                    {
                        album_info.uri = albumPath;
                    }

                    // TODO: handle pictures later
                    //checkAlbumPicture(album_info, path, ((size_t) index) + 1);
                    //scaleAlbumPicture(c, album_info);
                }
            }

            if (pic_size > size_embed)
            {
                path_embed = uri ;       // remember audio file with embedded picture
                size_embed = pic_size;
            }
        }
        while (theCursor.moveToNext());

        theCursor.close();

        Log.d(LOG_TAG, "DB: found " + album_info.no_tracks + " matching audio files");

        if ((album_info.pic_uri == null) && (extractFolderIcons))
        {
            Log.d(LOG_TAG, "DB: no folder picture found so far\n");
            if (size_embed > 0)
            {
                // TODO: extract?!?
                String path = getAlbumUri(path_embed);
                Log.d(LOG_TAG, "DB: path with embedded folder picture: " + path);
            }
        }
    }


    /******************************************************************************
     *
     * build albums database
     *
     * -> https://stackoverflow.com/questions/11219361/select-distinct-value-in-android-sqlite
     *
     *****************************************************************************/
    @SuppressWarnings("SameParameterValue")
    int runSqlCreateAllAlbumsFromFiles(HashMap<String, Uri> albumArtMap, boolean extractFolderIcons)
    {
        String[] columns = {FILES_TABLE_TCOL_ALBUM, FILES_TABLE_TCOL_ALBUM_ARTIST};
        String orderBy = FILES_TABLE_TCOL_ALBUM + ", " + FILES_TABLE_TCOL_ALBUM_ARTIST;
        Cursor theCursor = mDb.query(
                true,               // distinct
                FILES_TABLE_NAME,  // The table to query
                columns,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                orderBy,                                 // The sort order
                null            // limit
                );

        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return 0;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no files found");
            theCursor.close();
            return 0;
        }

        int nAlbums = 0;
        int album_id = 1;

        do
        {
            AudioAlbum info = new AudioAlbum();

            info.name = theCursor.getString(0);
            info.artist = theCursor.getString(1);

            Log.v(LOG_TAG, "DB: Found album \"" + info.name + "\" with performer \"" + info.artist + "\"");

            // fill out all members of the info object
            runSqlQueryGetAlbumInfoFromAudioFiles(0, info, extractFolderIcons);
            Log.v(LOG_TAG, "DB: found " + info.no_tracks + " tracks for this album");

            if (info.no_tracks > 0)
            {
                info.id = album_id;
                Uri picUri = albumArtMap.get(info.uri);      // get album art from files scan
                info.pic_uri = (picUri != null) ? picUri.toString() : null;      // store Uri as string
                ArrayList<AudioAlbum>albums = new ArrayList<>(1);
                albums.add(info);
                insertIntoAlbumsTable(albums);
                runSqlUpdateAlbumIdsInFilesTable(info);
                album_id++;
                nAlbums++;
            }
        }
        while (theCursor.moveToNext());
        theCursor.close();
        return nAlbums;
    }


    /* *****************************************************************************
     *
     * check if file exists in table and return information
     *
     *****************************************************************************/
    /*
    AudioFile runSqlQueryGetFileInfo
    (
        String uri
    )
    {
        final String where = FILES_TABLE_TCOL_PATH + " = ?";
        String[] whereVal = new String[]{uri};

        Cursor theCursor = mDb.query(
                false,              // distinct
                FILES_TABLE_NAME,           // table to query
                null,              // columns to return: all
                where,                      // columns for the WHERE clause
                whereVal,                   // values for the WHERE clause, replacing the '?'
                null,              // don't group the rows
                null,               // don't filter by row groups
                null,              // The sort order
                null                  // limit
        );

        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return null;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no file with that uri found");
            theCursor.close();
            return null;
        }

        int idColIndx          = theCursor.getColumnIndex(FILES_TABLE_TCOL_ID);
        int trackColIndx       = theCursor.getColumnIndex(FILES_TABLE_TCOL_TRACK_NO);
        int titleColIndx       = theCursor.getColumnIndex(FILES_TABLE_TCOL_TITLE);
        int albumColIndx       = theCursor.getColumnIndex(FILES_TABLE_TCOL_ALBUM);
        int albumIdColIndx     = theCursor.getColumnIndex(FILES_TABLE_TCOL_ALBUM_ID);
        int durationColIndx    = theCursor.getColumnIndex(FILES_TABLE_TCOL_DURATION);
        int groupingColIndx    = theCursor.getColumnIndex(FILES_TABLE_TCOL_GROUPING);
        int subtitleColIndx    = theCursor.getColumnIndex(FILES_TABLE_TCOL_SUBTITLE);
        int composerColIndx    = theCursor.getColumnIndex(FILES_TABLE_TCOL_COMPOSER);
        int performerColIndx   = theCursor.getColumnIndex(FILES_TABLE_TCOL_PERFORMER);
        int albumArtistColIndx = theCursor.getColumnIndex(FILES_TABLE_TCOL_ALBUM_ARTIST);
        int conductorColIndx   = theCursor.getColumnIndex(FILES_TABLE_TCOL_CONDUCTOR);
        int genreColIndx       = theCursor.getColumnIndex(FILES_TABLE_TCOL_GENRE);
        int yearColIndx        = theCursor.getColumnIndex(FILES_TABLE_TCOL_YEAR);
        int pathColIndx        = theCursor.getColumnIndex(FILES_TABLE_TCOL_PATH);
        int picTypeColIndx     = theCursor.getColumnIndex(FILES_TABLE_TCOL_PIC_TYPE);
        int picSizeColIndx     = theCursor.getColumnIndex(FILES_TABLE_TCOL_PIC_SIZE);
        int tagTypeColIndx     = theCursor.getColumnIndex(FILES_TABLE_TCOL_TAG_TYPE);
        int mtimeColIndx       = theCursor.getColumnIndex(FILES_TABLE_TCOL_MTIME);

        AudioFile info = new AudioFile();

        info.id           = theCursor.getInt(idColIndx);
        info.track_no     = theCursor.getInt(trackColIndx);
        info.title        = theCursor.getString(titleColIndx);
        info.album        = theCursor.getString(albumColIndx);
        info.album_id     = theCursor.getInt(albumIdColIndx);
        info.duration     = theCursor.getInt(durationColIndx);
        info.grouping     = theCursor.getString(groupingColIndx);
        info.subtitle     = theCursor.getString(subtitleColIndx);
        info.composer     = theCursor.getString(composerColIndx);
        info.performer    = theCursor.getString(performerColIndx);
        info.album_artist = theCursor.getString(albumArtistColIndx);
        info.conductor    = theCursor.getString(conductorColIndx);
        info.genre        = theCursor.getString(genreColIndx);
        info.year         = theCursor.getInt(yearColIndx);
        info.uri          = theCursor.getString(pathColIndx);
        info.pic_type     = theCursor.getInt(picTypeColIndx);
        info.pic_size     = theCursor.getInt(picSizeColIndx);
        info.tag_type     = theCursor.getInt(tagTypeColIndx);
        info.mtime        = theCursor.getLong(mtimeColIndx);

        theCursor.close();

        return info;
    }
     */


    /******************************************************************************
     *
     * run through all files in table and check if they are included in the map
     * and if the modification time matches
     *
     * return number of files that are not in the map or have
     * different mtime.
     *
     * Initialises mAudioFilesRemoved and mAudioFilesChanged.
     *
     * Removes unchanged files from mtimeMap so that only files to add will
     * remain there.
     *
     *****************************************************************************/
    int runSqlQueryCheckAllFiles
    (
        HashMap<String, FoundAudioFile> mtimeMap
    )
    {
        mAudioFilesRemoved = new ArrayList<>();
        mAudioFilesChanged = new ArrayList<>();

        Cursor theCursor = mDb.query(
                false,              // distinct
                FILES_TABLE_NAME,           // table to query
                null,              // columns to return: all
                null,              // columns for the WHERE clause
                null,           // values for the WHERE clause, replacing the '?'
                null,              // don't group the rows
                null,               // don't filter by row groups
                null,              // The sort order
                null                  // limit
        );

        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return -1;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no file found");
            theCursor.close();
            return 0;
        }

        int idColIndx          = theCursor.getColumnIndex(FILES_TABLE_TCOL_ID);
        int albumColIndx       = theCursor.getColumnIndex(FILES_TABLE_TCOL_ALBUM);
        int albumIdColIndx     = theCursor.getColumnIndex(FILES_TABLE_TCOL_ALBUM_ID);
        int albumArtistColIndx = theCursor.getColumnIndex(FILES_TABLE_TCOL_ALBUM_ARTIST);
        int pathColIndx        = theCursor.getColumnIndex(FILES_TABLE_TCOL_PATH);
        int mtimeColIndx       = theCursor.getColumnIndex(FILES_TABLE_TCOL_MTIME);

        do
        {
            String dbUri = theCursor.getString(pathColIndx);
            long dbMtime = theCursor.getLong(mtimeColIndx);
            FoundAudioFile af = mtimeMap.get(dbUri);

            if ((af == null) || (af.mtime != dbMtime))
            {
                // file does no longer exist or has changed

                AudioFileMinimumInfo info = new AudioFileMinimumInfo();
                info.id = theCursor.getInt(idColIndx);
                info.album = theCursor.getString(albumColIndx);
                info.album_id = theCursor.getInt(albumIdColIndx);
                info.album_artist = theCursor.getString(albumArtistColIndx);

                if (af != null)
                {
                    mAudioFilesChanged.add(info);
                }
                else
                {
                    mAudioFilesRemoved.add(info);
                }
            }
            else
            {
                Log.v(LOG_TAG, "file unchanged: " + dbUri);
                mtimeMap.remove(dbUri);         // remove from map
            }
        }
        while (theCursor.moveToNext());

        theCursor.close();

        return mAudioFilesChanged.size() + mAudioFilesRemoved.size();
    }


    /******************************************************************************
     *
     * info
     *
     *****************************************************************************/
    int getNumOfChangedFiles()
    {
        if (mAudioFilesChanged == null)
        {
            return -1;      // error
        }
        else
        {
            return mAudioFilesChanged.size();
        }
    }


    /******************************************************************************
     *
     * info
     *
     *****************************************************************************/
    int getNumOfDeletedFiles()
    {
        if (mAudioFilesRemoved == null)
        {
            return -1;      // error
        }
        else
        {
            return mAudioFilesRemoved.size();
        }
    }


    /******************************************************************************
     *
     * get album information
     *
     * return album info, if found
     *
     *****************************************************************************/
    AudioAlbum runSqlQueryAlbum
    (
        final String album_name,
        final String album_artist
    )
    {
        final String where;
        String[] whereVal;
        if ((album_artist != null) && !album_artist.isEmpty())
        {
            where = ALBUMS_TABLE_TCOL_NAME + " = ? AND " + ALBUMS_TABLE_TCOL_ARTIST + " = ?";
            whereVal = new String[]{album_name, album_artist};
        }
        else
        {
            where = ALBUMS_TABLE_TCOL_NAME + " = ? AND " + ALBUMS_TABLE_TCOL_ARTIST + " IS NULL";
            whereVal = new String[]{album_name};
        }

        Cursor theCursor = mDb.query(
                false,              // distinct
                ALBUMS_TABLE_NAME,           // table to query
                null,              // columns to return: all
                where,                      // columns for the WHERE clause
                whereVal,                   // values for the WHERE clause, replacing the '?'
                null,              // don't group the rows
                null,               // don't filter by row groups
                null,              // The sort order
                null                  // limit
        );

        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return null;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no file with that uri found");
            theCursor.close();
            return null;
        }

        int idColIndx        = theCursor.getColumnIndex(ALBUMS_TABLE_TCOL_ID);
        int nameColIndx      = theCursor.getColumnIndex(ALBUMS_TABLE_TCOL_NAME);
        int artistColIndx    = theCursor.getColumnIndex(ALBUMS_TABLE_TCOL_ARTIST);
        int composerColIndx  = theCursor.getColumnIndex(ALBUMS_TABLE_TCOL_COMPOSER);
        int performerColIndx = theCursor.getColumnIndex(ALBUMS_TABLE_TCOL_PERFORMER);
        int conductorColIndx = theCursor.getColumnIndex(ALBUMS_TABLE_TCOL_CONDUCTOR);
        int genreColIndx     = theCursor.getColumnIndex(ALBUMS_TABLE_TCOL_GENRE);
        int firstYearColIndx = theCursor.getColumnIndex(ALBUMS_TABLE_TCOL_FIRST_YEAR);
        int lastYearColIndx  = theCursor.getColumnIndex(ALBUMS_TABLE_TCOL_LAST_YEAR);
        int noTracksColIndx  = theCursor.getColumnIndex(ALBUMS_TABLE_TCOL_NO_TRACKS);
        int durationColIndx  = theCursor.getColumnIndex(ALBUMS_TABLE_TCOL_DURATION);
        int pathColIndx      = theCursor.getColumnIndex(ALBUMS_TABLE_TCOL_PATH);
        int artColIndx       = theCursor.getColumnIndex(ALBUMS_TABLE_TCOL_PICTURE);

        AudioAlbum info = new AudioAlbum();

        info.id         = theCursor.getInt(idColIndx);
        info.name       = theCursor.getString(nameColIndx);
        info.artist     = theCursor.getString(artistColIndx);
        info.composer   = theCursor.getString(composerColIndx);
        info.performer  = theCursor.getString(performerColIndx);
        info.conductor  = theCursor.getString(conductorColIndx);
        info.genre      = theCursor.getString(genreColIndx);
        info.first_year = theCursor.getInt(firstYearColIndx);
        info.last_year  = theCursor.getInt(lastYearColIndx);
        info.no_tracks  = theCursor.getInt(noTracksColIndx);
        info.duration   = theCursor.getInt(durationColIndx);
        info.uri        = theCursor.getString(pathColIndx);
        info.pic_uri    = theCursor.getString(artColIndx);

        theCursor.close();

        return info;
    }


    /******************************************************************************
     *
     * remove an audio file from the database
     *
     *****************************************************************************/
    private void removeAudioFileFromDatabase
    (
        AudioFileMinimumInfo info
    )
    {
        // 1. remove from music files table
        Log.v(LOG_TAG, "DB: remove file with id " + info.id + " from files table");
        runSqlRemoveFromFilesTable(info.id);
        // 2. delete corresponding album
        Log.v(LOG_TAG, "DB: remove album with id " + info.album_id + " from albums table");
        runSqlRemoveFromAlbumsTable(info.album_id);
        // 3. recreate corresponding album
        Log.v(LOG_TAG, "DB: recreate album with id=" + info.album_id + " name=\"" + info.album + "\", artist=\"" + info.album_artist + "\"");
        runSqlCreateAlbumFromFiles(info.album_id, info.album, info.album_artist, false);
    }


    /******************************************************************************
     *
     * remove deleted files from the database
     *
     *****************************************************************************/
    void removeDeletedAudioFilesFromDatabase()
    {
        if (mAudioFilesRemoved != null)
        {
            Log.v(LOG_TAG, "DB: about to remove " + mAudioFilesRemoved.size() + " audio files, that were deleted");
            for (AudioFileMinimumInfo info : mAudioFilesRemoved)
            {
                removeAudioFileFromDatabase(info);
            }
            mAudioFilesRemoved.clear();
        }
    }


    /******************************************************************************
     *
     * remove changed files from the database
     *
     *****************************************************************************/
    void removeChangedAudioFilesFromDatabase()
    {
        if (mAudioFilesChanged != null)
        {
            Log.v(LOG_TAG, "DB: about to remove " + mAudioFilesChanged.size() + " audio files, that were changed");
            for (AudioFileMinimumInfo info : mAudioFilesChanged)
            {
                removeAudioFileFromDatabase(info);
            }
            mAudioFilesChanged.clear();
        }
    }


    /******************************************************************************
     *
     * call before addAudioFileToDatabase() !
     *
     *****************************************************************************/
    private void prepareAddAudioFileToDatabase()
    {
        if (mNextAudioAlbumId == -1)
        {
            mNextAudioAlbumId = simpleQuery(ALBUMS_TABLE_NAME, "MAX(" + ALBUMS_TABLE_TCOL_ID + ")");
        }
        if (mNextAudioFileId == -1)
        {
            mNextAudioFileId = simpleQuery(FILES_TABLE_NAME, "MAX(" + FILES_TABLE_TCOL_ID + ")");
        }
    }


    /******************************************************************************
     *
     * add an audio file to the database
     *
     *****************************************************************************/
    void addAudioFileToDatabase
    (
        AudioFile info,
        boolean bExtractFolderIcons
    )
    {
        prepareAddAudioFileToDatabase();

        // 1. check if album already exists
        AudioAlbum album = runSqlQueryAlbum(info.album, info.album_artist);
        if (album != null)
        {
            // album exists, remember its id.
            info.album_id = (int) album.id;
            // 2a. remove it
            runSqlRemoveFromAlbumsTable(album.id);
        }
        else
        {
            // Album does not exist yet.
            // 2b. Invent album id.
            info.album_id = (int) ++mNextAudioAlbumId;
        }
        // 3. add new file
        info.id = (int) ++mNextAudioFileId;
        insertIntoFilesTable(info);

        // 4. recreate album with given id
        runSqlCreateAlbumFromFiles(info.album_id, info.album, info.album_artist, bExtractFolderIcons);
    }


    /************************************************************************************
     *
     * copy unique string
     *
     ***********************************************************************************/
    private static String strCpyUnique(String dst, String src)
    {
        final String sVarious = "≠";

        if (dst == null)
        {
            // not set before. Now dst may also be empty.
            return src;
        }

        if (!dst.startsWith(sVarious))
        {
            if ((src == null) || !src.equals(dst))
            {
                return sVarious;  // values differ
            }
        }

        return dst;
    }


    /******************************************************************************
     *
     * do not forget to close the database
     *
     *****************************************************************************/
    void close()
    {
        if (mDb != null)
        {
            mDb.close();
            mDb = null;
        }

    }

    void createAllTables()
    {
        if (mDb != null)
        {
            mDb.execSQL(SQL_CREATE_INFO_TABLE);
            mDb.execSQL(SQL_WRITE_INFO_TABLE);
            mDb.execSQL(SQL_CREATE_FILES_TABLE);
            mDb.execSQL(SQL_CREATE_ALBUMS_TABLE);
        }
    }

    void deleteAllTables()
    {
        if (mDb != null)
        {
            mDb.execSQL(SQL_DELETE_INFO_TABLE);
            mDb.execSQL(SQL_DELETE_FILES_TABLE);
            mDb.execSQL(SQL_DELETE_ALBUMS_TABLE);
        }
    }
}
