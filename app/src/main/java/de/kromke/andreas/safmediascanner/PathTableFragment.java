/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.safmediascanner;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

@SuppressWarnings("Convert2Lambda")
public class PathTableFragment extends Fragment
{
    private static final String LOG_TAG = "SMS : PathTableFragment";
    private RecyclerView mRecyclerView;
    private SafPathListAdapter mAdapter;
    private Button mButton;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    )
    {
        Log.d(LOG_TAG, "onCreateView()");
        MainActivity currActivity = getMainActivity();
        currActivity.onChangedFragment(1);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_path_table, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState)
    {
        Log.d(LOG_TAG, "onViewCreated()");
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = view.findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        // TODO: find better method than to access activity directly
        MainActivity activity = getMainActivity();
        /*
        String[] myDataset =
        {
            "eins",
            "zwei",
            "drei",
            "vier",
            "fünf",
            "sechs",
            "sieben",
            "acht",
            "neun",
            "zehn",
            "elf",
            "zwölf",
            "dreizehn",
            "vierzehn",
            "fünfzehn",
            "sechzehn",
            "siebzehn",
            "achtzehn",
            "neunzehn"
        };
        */

        mButton = view.findViewById(R.id.button_goto_scan);
        onChangedPathListSelection(false);
        if (activity != null)
        {
            mAdapter = new SafPathListAdapter(activity.mPathArrayList, activity.mSelectedIndex, getContext());
            mRecyclerView.setAdapter(mAdapter);
            mButton.setEnabled(activity.mSelectedIndex >= 0);

            mButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    NavHostFragment.findNavController(PathTableFragment.this)
                            .navigate(R.id.action_FirstFragment_to_SecondFragment);
                }
            });
        }
    }


    /************************************************************************************
     *
     * helper
     *
     ***********************************************************************************/
    private MainActivity getMainActivity()
    {
        return (MainActivity) getActivity();
    }


    /************************************************************************************
     *
     * TODO: find better solution for this
     *
     ***********************************************************************************/
    void onChangedPathList(ArrayList<String> newArray, int selectedIndex)
    {
        mAdapter = new SafPathListAdapter(newArray, selectedIndex, getContext());
        mRecyclerView.setAdapter(mAdapter);
    }


    /************************************************************************************
     *
     * TODO: find better solution for this
     *
     ***********************************************************************************/
    void onChangedPathListSelection(boolean bSelected)
    {
        mButton.setEnabled(bSelected);
    }
}
