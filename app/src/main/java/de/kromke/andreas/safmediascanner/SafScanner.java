/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.safmediascanner;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;

import static java.lang.Math.abs;
import static java.lang.System.currentTimeMillis;


class SafScanner
{
    private AsyncTask<Integer, Integer, Integer> mRunningTask = null;              // make threading safe
    private enum modeType
    {
        rebuildAll,                 // scan all directories and build new database
        countAudioFiles             // scan all directories and count audio files
    }
    private static final int scanRebuildAll = 1;             // scan all directories and build new database
    private static final int scanCountAudioFiles = 2;        // scan all directories and count audio files
    private static final int checkDbFiles = 3;               // scan database for deleted and changed files
    private static final int removeDbFiles = 4;              // remove deleted and changed files
    private static final int addDbFiles = 5;                 // add new and changed files
    private static final int incremental = 6;                // run modes 2..5

    private static boolean mbUseNativeTagger = true;
    private static boolean mbVerifyTagReader = false;

    private static final String LOG_TAG = "SMS : SafScanner";
    public static final String sDbTempName = "safmetadata_temp.db";
    public static final String sAlbumArtFileName = "albumart";
    @SuppressWarnings("FieldCanBeLocal")
    private static final int recurseLimit = 8;        // TODO: increase after test
    @SuppressWarnings("FieldCanBeLocal")
    private static final int filesLimit = 99999;        // TODO: increase after test
    @SuppressWarnings("FieldCanBeLocal")
    private final Context mContext;
    private final ContentResolver mResolver;
    @SuppressWarnings("FieldCanBeLocal")
    //private int mProcessId;
    private MusicDatabase mDataBase;
    private final DocumentFile mSafTree;
    private File mDatabaseFile;         // new db: file in public internal memory, updating db: temporary file in private internal memory
    private boolean mbCopyDbToSafPath;
    private boolean mbExtractFolderIcons;
    private boolean mbScaleExistingImages;
    private boolean mbKeepBackup;
    private boolean mbRenameExistingImages;
    private int mMaxFolderImageSize;
    private final DocumentFile mSharedDbPath;       // maybe null
    private final String mPrivateDbPath;
    private final String mDbName;
    private final String mSuggestedDbNameInSharedPath;

    private modeType mMode;
    private long mStartTime;
    private HashMap<String, Uri> mAlbumArtMap = null;
    private HashMap<String, MusicDatabase.FoundAudioFile> mFoundFilesMap = null;
    private final NativeTaggerInterface mNativeTagger = new NativeTaggerInterface();
    private final MyTaggerInterface mMyTagger = new MyTaggerInterface();
    boolean mbProgressTextChanged = false;
    String mProgressText = "";
    private int mId;        // audio file id, gets incremented
    //private boolean mbFileError = false;

    // for testing
    private boolean mbSimulateDeletionAndChange;
    Random mRandom = new Random(currentTimeMillis());


    /**************************************************************************
     *
     * constructor
     *
     *************************************************************************/
    SafScanner
    (
        Context context,
        final DocumentFile sharedDbPath,
        final String suggestedFilenameInSharedDbPath,
        final String privateDbPath,
        final DocumentFile treeToScan
    )
    {
        if (sharedDbPath != null)
        {
            Log.d(LOG_TAG, "SafScanner(): shared db Uri: " + sharedDbPath.getUri());
        }
        Log.d(LOG_TAG, "SafScanner(): private db path: " + privateDbPath);
        /*
        File dir = new File(sharedDbPath);
        if (!dir.isDirectory() && !dir.mkdirs())
        {
            mbFileError = true;
            Log.e(LOG_TAG, "getMusicBasePath(): cannot create directories: " + sharedDbPath);
        }
        else
        {
            Log.d(LOG_TAG, "getMusicBasePath(): created directories: " + sharedDbPath);
        }
        */
        mContext = context;
        mResolver = mContext.getContentResolver();
        mSharedDbPath = sharedDbPath;       // maybe null
        mSuggestedDbNameInSharedPath = suggestedFilenameInSharedDbPath;
        mPrivateDbPath = privateDbPath;
        mDbName = MainActivity.sDbName;
        mSafTree = treeToScan;

        //copyDatabaseFileToPrivateMemory(sharedDbPath, null, mDbName);
    }


    /**************************************************************************
     *
     * check if background task is running
     *
     *************************************************************************/
    public boolean isBusy()
    {
        return (mRunningTask != null);
    }


    /**************************************************************************
     *
     * settings
     *
     *************************************************************************/
    void applySettings
    (
        boolean bCopyDbToSafPath,
        boolean bExtractFolderIcons,
        boolean bScaleExistingImages,
        boolean bKeepBackup,
        boolean bRenameExistingImages,
        boolean bUseNativeTaglib,
        boolean bVerifyTagReader,
        int maxFolderImageSize
    )
    {
        mbCopyDbToSafPath = bCopyDbToSafPath;
        mbExtractFolderIcons = bExtractFolderIcons;
        mbScaleExistingImages = bScaleExistingImages;
        mbKeepBackup = bKeepBackup;
        mbRenameExistingImages = bRenameExistingImages;
        mbUseNativeTagger = bUseNativeTaglib;
        mbVerifyTagReader = bVerifyTagReader;
        mMaxFolderImageSize = maxFolderImageSize;
    }


    /**************************************************************************
     *
     * runner
     *
     *************************************************************************/
    void runTotalScan
    (
        boolean bSimulateDeletionAndChange
    )
    {
        if (mRunningTask != null)
        {
            Log.e(LOG_TAG, "runTotalScan() : scanner busy");
            return;
        }

        mbSimulateDeletionAndChange = bSimulateDeletionAndChange;

        mDatabaseFile = new File(mPrivateDbPath, mDbName);
        mDataBase = new MusicDatabase(mPrivateDbPath, mDbName, true);
        if (mDataBase.exists())
        {
            mDataBase.deleteAllTables();
            mDataBase.createAllTables();

            //mProcessId = android.os.Process.myPid();
            //Log.d(LOG_TAG, "SafScanner() : processId is " + mProcessId);
            mRunningTask = new FileAccessTask(mSafTree).execute(scanRebuildAll);
        }
        else
        {
            mProgressText = "FATAL: access error on " + mDatabaseFile;
            mbProgressTextChanged = true;
        }
    }


    /**************************************************************************
     *
     * runner
     *
     *************************************************************************/
    void runIncrementalScan
    (
        boolean bSimulateDeletionAndChange
    )
    {
        if (mRunningTask != null)
        {
            Log.e(LOG_TAG, "runIncrementalScan() : scanner busy");
            return;
        }

        mbSimulateDeletionAndChange = bSimulateDeletionAndChange;

        // 1. open existing database
        runOpenExistingDatabase();
        if (mDataBase == null)
        {
            return;     // failure
        }

        mRunningTask = new FileAccessTask(mSafTree).execute(incremental);
    }


    /**************************************************************************
     *
     * runner
     *
     *************************************************************************/
    void runCountFiles()
    {
        if (mRunningTask != null)
        {
            Log.e(LOG_TAG, "runCountFiles() : scanner busy");
            return;
        }

        mRunningTask = new FileAccessTask(mSafTree).execute(scanCountAudioFiles);
    }


    /**************************************************************************
     *
     * runner
     *
     *************************************************************************/
    void runOpenExistingDatabase()
    {
        if (mRunningTask != null)
        {
            Log.e(LOG_TAG, "runOpenExistingDatabase() : scanner busy");
            return;
        }

        if (mDataBase == null)
        {
            mProgressText += "\n ======= RETRIEVING DATABASE =======\n\n";

            int result = copyDatabaseFileToPrivateMemory(mSafTree, MainActivity.sDbPath, MainActivity.sDbName);  // deactivate for debugging
            if (result == 0)
            {
                mProgressText += "Database file copied from music location\n";
            }
            else
            {
                result = copyDatabaseFileToPrivateMemory(mSharedDbPath, null, mSuggestedDbNameInSharedPath);
                if (result == 0)
                {
                    mProgressText += "Database file copied from shared directory\n";
                }
            }
            //int result = 0; // getDatabaseFileFromSaf(mSharedDbPath);
            //mDatabaseFile = new File(mSharedDbPath, sDbTempName);  // activate for debugging

            boolean bSuccess = false;
            if (result == 0)
            {
                mDataBase = new MusicDatabase(mDatabaseFile.getParent(), mDatabaseFile.getName(), false);
                if (mDataBase.exists())
                {
                    int nFiles = mDataBase.getNumberOfFiles();
                    int nAlbums = mDataBase.getNumberOfAlbums();
                    mProgressText += "Database contains " + nFiles + " files in " + nAlbums + " albums\n";
                    bSuccess = true;
                } else
                {
                    mDataBase.close();
                    mDataBase = null;
                    mProgressText += "fatal error on copied database\n";
                }
            } else if (result == 1)
            {
                mProgressText += "No database found\n";
            } else
            {
                mProgressText += "fatal error\n";
            }

            String finalText = (bSuccess) ? "DATABASE RETRIEVED" : "NO DATABASE RETRIEVED";
            mProgressText += "\n ======= " + finalText + " =======\n\n";
            mbProgressTextChanged = true;
        }
    }


    /**************************************************************************
     *
     * runner
     *
     *************************************************************************/
    void runCheckDatabaseFiles()
    {
        if (mRunningTask != null)
        {
            Log.e(LOG_TAG, "runCheckDatabaseFiles() : scanner busy");
            return;
        }

        if (mDataBase != null)
        {
            if (mFoundFilesMap != null)
            {
                mRunningTask = new FileAccessTask(mSafTree).execute(checkDbFiles);
            }
            else
            {
                mProgressText += "first search for files!\n";
                mbProgressTextChanged = true;
            }
        }
        else
        {
            mProgressText += "database not opened\n";
            mbProgressTextChanged = true;
        }
    }


    /**************************************************************************
     *
     * runner
     *
     *************************************************************************/
    void runRemoveDatabaseFiles()
    {
        if (mRunningTask != null)
        {
            Log.e(LOG_TAG, "runRemoveDatabaseFiles() : scanner busy");
            return;
        }

        if (mDataBase != null)
        {
            int nc = mDataBase.getNumOfChangedFiles();
            int nd = mDataBase.getNumOfDeletedFiles();
            if ((nc > 0) || (nd > 0))
            {
                mRunningTask = new FileAccessTask(mSafTree).execute(removeDbFiles);
            }
            else
            if ((nc == 0) && (nd == 0))
            {
                mProgressText += "nothing to do\n";
                mbProgressTextChanged = true;
            }
            else
            {
                mProgressText += "Run check first!\n";
                mbProgressTextChanged = true;
            }
        }
        else
        {
            mProgressText += "database not opened\n";
            mbProgressTextChanged = true;
        }
    }


    /**************************************************************************
     *
     * runner
     *
     *************************************************************************/
    void runAddDatabaseFiles()
    {
        if (mRunningTask != null)
        {
            Log.e(LOG_TAG, "runAddDatabaseFiles() : scanner busy");
            return;
        }

        if (mDataBase != null)
        {
            int nc = mDataBase.getNumOfChangedFiles();
            int nd = mDataBase.getNumOfDeletedFiles();
            if ((nc > 0) || (nd > 0))
            {
                mRunningTask = new FileAccessTask(mSafTree).execute(addDbFiles);
            }
            else
            if ((mFoundFilesMap == null) || (mFoundFilesMap.size() == 0))
            {
                mProgressText += "nothing to do\n";
                mbProgressTextChanged = true;
            }
            else
            {
                mProgressText += "Run check first!\n";
                mbProgressTextChanged = true;
            }
        }
        else
        {
            mProgressText += "database not opened\n";
            mbProgressTextChanged = true;
        }
    }


    /**************************************************************************
     *
     * scale album picture
     *
     *************************************************************************/
    @SuppressWarnings("UnusedReturnValue")
    private DocumentFile scalePicture(DocumentFile df)
    {
        ImageScaler scaler = new ImageScaler();
        int rc = scaler.open(mResolver, df);
        if (rc == 0)
        {
            rc = scaler.scale(mMaxFolderImageSize, mbKeepBackup);
            if (rc > 0)
            {
                return scaler.mScaledFile;
            }
        }

        return df;      // return original image
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    void verifyStr(final String s1, final String s2, final String info)
    {
        if ((s1 == null) && (s2 == null))
        {
            return;
        }
        if ((s1 == null) || !(s1.equals(s2)))
        {
            Log.e(LOG_TAG, "verifyStr() : mismatch in " + info + ": \"" + s1 + "\" (native) <==> \"" + s2 + "\" (my)");
        }
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    void verifyNum(int n1, int n2, final String info)
    {
        if (n1 != n2)
        {
            Log.e(LOG_TAG, "verifyNum() : mismatch in " + info + ": " + n1 + " (native) <==> " + n2 + " (my)");
        }
    }


    /**************************************************************************
     *
     * get metadata from audio file
     *
     *************************************************************************/
    private MusicDatabase.AudioFile handleAudioFile(final DocumentFile df, boolean bIncremental)
    {
        mProgressText += df.getName() + "\n";
        mbProgressTextChanged = true;

        MusicDatabase.AudioFile af;
        if (mbUseNativeTagger)
        {
            af = mNativeTagger.getTags(df, mResolver);
        }
        else
        {
            af = mMyTagger.getTags(df, mResolver);
        }

        if (af != null)
        {
            // insert into db
            if (bIncremental)
            {
                // insert file and maintain existing resp. create new album
                mDataBase.addAudioFileToDatabase(af, mbExtractFolderIcons);
            }
            else
            {
                // just add now to files, list, create albums later
                af.id = mId++;
                mDataBase.insertIntoFilesTable(af);
            }
        }

        if (mbVerifyTagReader && mbUseNativeTagger)
        {
            MusicDatabase.AudioFile af2 = mMyTagger.getTags(df, mResolver);
            if ((af != null) && (af2 != null))
            {
                verifyNum(af.track_no, af2.track_no, "track number");
                verifyNum(af.year, af2.year, "year");
                verifyNum(af.pic_size, af2.pic_size, "picture size");
                verifyNum(af.pic_type, af2.pic_type, "picture type");
                verifyNum(af.tag_type, af2.tag_type, "tag type");
                if (abs(af.duration - af2.duration) > 1)
                {
                    int n1 = af.duration;
                    int n2 = af2.duration;
                    int diff_ms = n2 - n1;
                    Log.e(LOG_TAG, "verifyNum() : " + diff_ms + " ms duration mismatch: " + n1 + " (native) <==> " + n2 + " (my) in file: " + df.getName());
                }

                verifyStr(af.title, af2.title, "title (" + df.getName() + ")");
                verifyStr(af.album, af2.album, "album (" + df.getName() + ")");
                verifyStr(af.grouping, af2.grouping, "grouping (" + df.getName() + ")");
                verifyStr(af.subtitle, af2.subtitle, "subtitle (" + df.getName() + ")");
                verifyStr(af.composer, af2.composer, "composer (" + df.getName() + ")");
                verifyStr(af.performer, af2.performer, "artist (" + df.getName() + ")");
                verifyStr(af.album_artist, af2.album_artist, "album artist (" + df.getName() + ")");
                verifyStr(af.conductor, af2.conductor, "conductor (" + df.getName() + ")");
                verifyStr(af.genre, af2.genre, "genre (" + df.getName() + ")");
                verifyStr(af.uri, af2.uri, "uri (" + df.getName() + ")");
            }
        }

        return af;
    }


    /**************************************************************************
     *
     * extract picture from audio file.
     *
     * returns created picture or null
     *
     *************************************************************************/
    private DocumentFile extractPicture(final DocumentFile dfAudioFile, @NonNull final String mime)
    {
        // create picture file
        DocumentFile dd = dfAudioFile.getParentFile();
        if (dd == null)
        {
            Log.e(LOG_TAG, "extractPicture() : cannot get parent of: \"" + dfAudioFile.getUri().getPath() + "\"");
            return null;
        }

        DocumentFile dfPicture = dd.createFile(mime, sAlbumArtFileName);
        if (dfPicture == null)
        {
            Log.e(LOG_TAG, "extractPicture() : cannot create file in: \"" + dd.getUri().getPath() + "\"");
            return null;
        }

        String path = dfAudioFile.getUri().getPath();
        mbProgressTextChanged = true;

        int rc;
        if (mbUseNativeTagger)
        {
            rc = mNativeTagger.extractPicture(dfAudioFile, mResolver, path, dfAudioFile.length(), dfAudioFile.lastModified(), dfPicture);
        }
        else
        {
            rc = mMyTagger.extractPicture(dfAudioFile, mResolver, path, dfAudioFile.length(), dfAudioFile.lastModified(), dfPicture);
        }
        if (rc < 0)
        {
            mProgressText += "extraction failed: embedded picture from " + dfAudioFile.getName() + "\n";
            return null;
        }

        if (mbVerifyTagReader & mbUseNativeTagger)
        {
            rc = mMyTagger.extractPicture(dfAudioFile, mResolver, path, dfAudioFile.length(), dfAudioFile.lastModified(), dfPicture);
            if (rc < 0)
            {
                mProgressText += "extraction failed: embedded picture from " + dfAudioFile.getName() + "\n";
                return null;
            }
        }

        mProgressText += "extract embedded picture from " + dfAudioFile.getName() + "\n";
        return scalePicture(dfPicture);
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private static void closeStream(Closeable s)
    {
        try
        {
            s.close();
        }
        catch (Exception e)
        {
            Log.e(LOG_TAG, "I/O exception");
        }
    }


    /************************************************************************************
     *
     * helper
     *
     ***********************************************************************************/
    private MainActivity getMainActivity()
    {
        return MainActivity.sInstance;
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private static boolean copyFileFromTo(InputStream is, OutputStream os)
    {
        boolean result = true;
        try
        {
            byte[] buffer = new byte[4096];
            int length;
            while ((length = is.read(buffer)) > 0)
            {
                os.write(buffer, 0, length);
            }
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "file not found");
            result = false;
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "I/O exception");
            result = false;
        } finally
        {
            if (is != null)
                closeStream(is);
            if (os != null)
                closeStream(os);
        }

        return result;
    }


    /**************************************************************************
     *
     * Copy database file from SAF path to application-private memory
     *
     * 0: OK (mDatabaseFile now valid)
     * 1: not found
     * -1: fatal error
     *
     *************************************************************************/
    private int copyDatabaseFileToPrivateMemory
    (
        DocumentFile srcFileTree,
        String srcDirectory,
        String srcFilename
    )
    {
        if (srcFileTree == null)
        {
            return 1;
        }
        mDatabaseFile = null;       // pessimistic...

        DocumentFile ddf;
        if (srcDirectory != null)
        {
            ddf = srcFileTree.findFile(srcDirectory);
            if (ddf == null)
            {
                Log.e(LOG_TAG, "getDatabaseFileFromSaf() : cannot find directory " + srcDirectory);
                return 1;
            }
        }
        else
        {
            ddf = srcFileTree;
        }

        DocumentFile df = ddf.findFile(srcFilename);
        if (df == null)
        {
            if (srcDirectory != null)
            {
                Log.e(LOG_TAG, "cannot find db file " + srcFilename + " in directory " + srcDirectory);
            }
            else
            {
                Log.e(LOG_TAG, "cannot find db file " + srcFilename + " in shared db path");
            }
            return 1;
        }

        InputStream is;
        try
        {
            is = mResolver.openInputStream(df.getUri());
            if (is == null)
            {
                Log.e(LOG_TAG, "cannot open file for input " + srcFilename + " in directory " + srcDirectory);
                return -1;
            }
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "cannot create output stream");
            return -1;
        }

        boolean bResult = false;
        File tempDbFile = new File(mPrivateDbPath, sDbTempName);
        //noinspection ResultOfMethodCallIgnored
        tempDbFile.delete();
        FileOutputStream os;
        try
        {
            if (!tempDbFile.createNewFile())
            {
                Log.e(LOG_TAG, "cannot create temporary file");
                closeStream(is);
                return -1;
            }
            os = new FileOutputStream(tempDbFile);
            bResult = copyFileFromTo(is, os);
        } catch (IOException e)
        {
            closeStream(is);
            Log.e(LOG_TAG, "cannot create temporary file: " + e);
        }

        if (bResult)
        {
            Log.d(LOG_TAG,"getDatabaseFileFromSaf() : Database retrieved");
            mDatabaseFile = tempDbFile;
            return 0;
        }
        else
        {
            Log.e(LOG_TAG,"getDatabaseFileFromSaf() : Database not retrieved");
            return -1;
        }
    }


    /**************************************************************************
     *
     * Copy database file from internal memory to SAF path
     *
     *************************************************************************/
    private boolean copyDbToSafPath
    (
        DocumentFile destFileTree,
        String destDirectory,
        @SuppressWarnings("SameParameterValue") String destFilename
    )
    {
        if (destFileTree == null)
        {
            return false;
        }

        //
        // open destination directory, create if necessary
        //

        DocumentFile ddf;
        if (destDirectory != null)
        {
            ddf = destFileTree.findFile(destDirectory);
            if (ddf != null)
            {
                if (!ddf.isDirectory())
                {
                    // exists, but is no directory: fatal failure
                    return false;
                }
            } else
            {
                // does not exist
                ddf = destFileTree.createDirectory(destDirectory);
                if (ddf == null)
                {
                    // cannot create: fatal failure
                    return false;
                }
            }
        }
        else
        {
            ddf = destFileTree;
        }

        //
        // open source file for reading
        //

        FileInputStream is;
        try
        {
            is = new FileInputStream(mDatabaseFile);
        } catch (FileNotFoundException e)
        {
            // cannot open source for reading: fatal failure
            return false;
        }

        //
        // remove old destination file, if exists
        //

        DocumentFile df = ddf.findFile(destFilename);
        if (df != null)
        {
            if (!df.isFile() || !df.delete())
            {
                // destination file exists, but is no file: fatal failure
                // destination file exists and is file, but could not delete: fatal failure
                closeStream(is);
                return false;
            }
        }

        //
        // create a new destination file
        //

        df = ddf.createFile("application/vnd.sqlite3", destFilename);
        if (df == null)
        {
            // cannot create destination file: fatal failure
            closeStream(is);
            return false;
        }

        //
        // copy data
        //

        OutputStream os;
        try
        {
            os = mResolver.openOutputStream(df.getUri());
            return copyFileFromTo(is, os);
        } catch (FileNotFoundException e)
        {
            closeStream(is);
            Log.e(LOG_TAG, "cannot create output stream");
            return false;
        }
    }


    /**************************************************************************
     *
     * async task for file operations
     *
     *************************************************************************/
    @SuppressLint("StaticFieldLeak")
    private class FileAccessTask extends AsyncTask<Integer /* do */, Integer /* pre */, Integer /* post */>
    {
        int actionCode = 0;
        private final DocumentFile mDocumentFileTree;
        int mNumAudioFilesFound = 0;
        int mNumDirectories = 0;
        int mNumPicturesRenamed = 0;

        FileAccessTask(DocumentFile df)
        {
            mDocumentFileTree = df;
        }

        @Override
        protected void onPreExecute()
        {
            Log.d(LOG_TAG, "onPreExecute()");
            mId = 1;
        }


        /**************************************************************************
         *
         * main scan function, runs recursively
         *
         * Gathers audio files into the database.
         * In parallel, gather album art.
         *
         * In incremental scan mode: Gather files in memory, because SAF is so awfully
         * slow that a second path would be slower than a complete database rebuild.
         *
         *************************************************************************/
        private void scan(DocumentFile df, int recursionCnt)
        {
            Log.d(LOG_TAG, "scan() : subdirectory \"" + df.getName() + "\" entered.");
            DocumentFile albumArt = null;   // "best" album art file
            int albumArtPrio = -1;
            final int prioHighest = 1000;
            DocumentFile bestWithEmbeddedAlbumArtDf = null;  // "best" embedded album art
            MusicDatabase.AudioFile bestWithEmbeddedAlbumArtAf = null;  // "best" embedded album art

            mProgressText += df.getName() + "/\n";
            mbProgressTextChanged = true;

            DocumentFile[] children = df.listFiles();
            for (DocumentFile child : children)
            {
                if (mNumAudioFilesFound >= filesLimit)
                {
                    Log.w(LOG_TAG, "scan() : max num of files reached (" + filesLimit + ")");
                    continue;
                }

                String name = child.getName();
                if (name == null)
                {
                    Log.e(LOG_TAG, "scan() : null name?!?");
                    continue;
                }

                String nameLowerCase = name.toLowerCase();

                if (child.isDirectory())
                {
                    if (recursionCnt > recurseLimit)
                    {
                        Log.w(LOG_TAG, "scan() : subdirectory " + child.getName() + " ignored because recursion limit is exceeded.");
                    }
                    else
                    {
                        scan(child, recursionCnt + 1);  // recursion
                    }
                }
                else
                if (name.startsWith("."))
                {
                    Log.w(LOG_TAG, "scan() : file \"" + name + "\" ignored because name starts with '.'.");
                }
                else
                if (nameLowerCase.endsWith(".mp3") ||
                    nameLowerCase.endsWith(".mp4") ||
                    nameLowerCase.endsWith(".m4a") ||
                    nameLowerCase.endsWith(".flac") ||
                    nameLowerCase.endsWith(".opus") ||
                    nameLowerCase.endsWith(".ogg"))
                {
                    Log.d(LOG_TAG, "scan() : audio file \"" + name + "\" found.");
                    if (mMode == modeType.rebuildAll)
                    {
                        if (mbSimulateDeletionAndChange && (mRandom.nextInt() % 13 == 3))
                        {
                            Log.w(LOG_TAG, "scan() : TESTING: randomly skipped file: " + child.getUri().toString());
                        }
                        else
                        {
                            MusicDatabase.AudioFile af = handleAudioFile(child, false);
                            if (af != null)
                            {
                                if (af.pic_size > 0)
                                {
                                    // get "best" embedded album art
                                    if (bestWithEmbeddedAlbumArtAf == null)
                                    {
                                        // this is the first file with embedded album art
                                        bestWithEmbeddedAlbumArtAf = af;
                                        bestWithEmbeddedAlbumArtDf = child;
                                    } else if (af.pic_size > bestWithEmbeddedAlbumArtAf.pic_size)
                                    {
                                        // not the first, but a better file
                                        bestWithEmbeddedAlbumArtAf = af;
                                        bestWithEmbeddedAlbumArtDf = child;
                                    }
                                }
                                mNumAudioFilesFound++;
                            }
                        }
                    }
                    else
                    if (mMode == modeType.countAudioFiles)
                    {
                        // gather, together with modification date
                        if (mbSimulateDeletionAndChange && (mRandom.nextInt() % 11 == 5))
                        {
                            Log.w(LOG_TAG, "scan() : TESTING: randomly skipped file: " + child.getUri().toString());
                        }
                        else
                        if (mbSimulateDeletionAndChange && (mRandom.nextInt() % 11 == 5))
                        {
                            MusicDatabase.FoundAudioFile af = new MusicDatabase.FoundAudioFile();
                            af.df = child;
                            Log.w(LOG_TAG, "scan() : TESTING: randomly changed mtime for file: " + child.getUri().toString());
                            af.mtime = 4711;
                            mFoundFilesMap.put(child.getUri().toString(), af);
                            //noinspection StringConcatenationInLoop
                            mProgressText += child.getName() + "\n";
                            mbProgressTextChanged = true;
                            mNumAudioFilesFound++;
                        }
                        else
                        {
                            MusicDatabase.FoundAudioFile af = new MusicDatabase.FoundAudioFile();
                            af.df = child;
                            af.mtime = child.lastModified();
                            mFoundFilesMap.put(child.getUri().toString(), af);
                            //noinspection StringConcatenationInLoop
                            mProgressText += child.getName() + "\n";
                            mbProgressTextChanged = true;
                            mNumAudioFilesFound++;
                        }
                    }
                }
                else
                if (nameLowerCase.equals(sAlbumArtFileName + ".jpg"))
                {
                    albumArt = child;
                    albumArtPrio = prioHighest;
                }
                else
                if  (nameLowerCase.equals("folder.jpg") && (albumArtPrio < 2))
                {
                    albumArt = child;
                    albumArtPrio = 2;
                }
                else
                if  (nameLowerCase.equals("folder.png") && (albumArtPrio < 1))
                {
                    albumArt = child;
                    albumArtPrio = 1;
                }
                else
                if (nameLowerCase.equals("cover.jpg") && (albumArtPrio < 0))
                {
                    albumArt = child;
                    albumArtPrio = 0;
                }
                else
                {
                    Log.d(LOG_TAG, "scan() : file \"" + name + "\" ignored because not detected as audio or picture.");
                }
            }

            //
            // Handle album art for this directory. If a picture file exists, take it,
            // otherwise try to extract one.
            //

            DocumentFile pictureFile = null;
            if (albumArt != null)
            {
                Log.d(LOG_TAG, "scan() : album art detected: \"" + albumArt.getName() + "\".");
                if (mbScaleExistingImages && (mMode == modeType.rebuildAll))
                {
                    pictureFile = scalePicture(albumArt);
                }
                else
                {
                    pictureFile = albumArt;
                }

                if (mbRenameExistingImages && (albumArtPrio != prioHighest))
                {
                    // rename picture
                    final String newname = sAlbumArtFileName + ".jpg";
                    if (pictureFile.renameTo(newname))
                    {
                        mNumPicturesRenamed++;
                        // hopefully pictureFile reflects the new name?!?
                        final String currName = pictureFile.getName();
                        Log.d(LOG_TAG, "scan() : renamed to \"" + currName + "\"");
                        if ((currName == null) || (!currName.equalsIgnoreCase(newname)))
                        {
                            Log.e(LOG_TAG, "scan() : wrong rename result: \"" + currName + "\"");
                        }
                    }
                    else
                    {
                        Log.e(LOG_TAG, "scan() : cannot rename \"" + pictureFile.getName() + "to \"" + newname + "\"");
                    }
                }
            }
            else
            if (bestWithEmbeddedAlbumArtDf != null)
            {
                Log.d(LOG_TAG, "scan() : embedded album art detected in: \"" + bestWithEmbeddedAlbumArtDf.getName() + "\".");
                if (mbExtractFolderIcons)
                {
                    String mime = (bestWithEmbeddedAlbumArtAf.pic_type == 1) ? "image/jpeg" : "image/png";
                    pictureFile = extractPicture(bestWithEmbeddedAlbumArtDf, mime);
                }
            }

            if ((pictureFile != null) && (mMode == modeType.rebuildAll))
            {
                mAlbumArtMap.put(df.getUri().toString(), pictureFile.getUri());
            }

            mNumDirectories++;
        }


        /**************************************************************************
         *
         * helper
         *
         *************************************************************************/
        private void setProgressTextAfterScan()
        {
            mProgressText += "\n ======= " + mNumDirectories + " directories scanned =======";
            mProgressText += "\n ======= " + mNumAudioFilesFound + " files processed =======";
            if (mNumPicturesRenamed != 0)
            {
                mProgressText += "\n ======= " + mNumPicturesRenamed + " picture file(s) renamed =======";
            }
        }


        /**************************************************************************
         *
         * run scan in mode "rebuild all"
         *
         *************************************************************************/
        private int runScanRebuildAll()
        {
            Log.d(LOG_TAG, "doInBackground() : scanning " + mDocumentFileTree.getUri().getPath());

            mAlbumArtMap = new HashMap<>();
            mFoundFilesMap = null;

            mMode = modeType.rebuildAll;
            scan(mDocumentFileTree, 0);
            setProgressTextAfterScan();
            int nAlbums = mDataBase.runSqlCreateAllAlbumsFromFiles(mAlbumArtMap, false);
            mProgressText += "\n ======= " + nAlbums + " albums created =======\n";

            Log.d(LOG_TAG, "doInBackground() : " + mNumAudioFilesFound + " audio files found");
            return mNumAudioFilesFound;
        }


        /**************************************************************************
         *
         * run scan in mode "count audio files"
         *
         *************************************************************************/
        private int runScanCountAudioFiles()
        {
            Log.d(LOG_TAG, "doInBackground() : scanning " + mDocumentFileTree.getUri().getPath());

            mAlbumArtMap = null;
            mFoundFilesMap = new HashMap<>();

            mMode = modeType.countAudioFiles;
            scan(mDocumentFileTree, 0);
            setProgressTextAfterScan();

            Log.d(LOG_TAG, "doInBackground() : " + mNumAudioFilesFound + " audio files found");
            return mNumAudioFilesFound;
        }


        /**************************************************************************
         *
         * AsyncTask method
         *
         *************************************************************************/
        @Override
        protected Integer doInBackground(Integer... params)
        {
            actionCode = params[0];
            Log.d(LOG_TAG, "doInBackground(" + actionCode + ")");
            int result = -1;
            mStartTime = currentTimeMillis();

            switch (actionCode)
            {
                case scanRebuildAll:
                    result = runScanRebuildAll();
                    break;

                case incremental:
                    runScanCountAudioFiles();
                    int num1 = mDataBase.runSqlQueryCheckAllFiles(mFoundFilesMap);
                    mProgressText += "\n ======= " + num1 + " deleted or changed files found =======";
                    mbProgressTextChanged = true;
                    mDataBase.removeDeletedAudioFilesFromDatabase();
                    mDataBase.removeChangedAudioFilesFromDatabase();
                    mProgressText += "\n ======= adding new or changed files =======\n\n";
                    mbProgressTextChanged = true;
                    int num2 = 0;
                    for (Map.Entry<String, MusicDatabase.FoundAudioFile> entry : mFoundFilesMap.entrySet())
                    {
                        handleAudioFile(entry.getValue().df, true);
                        num2++;
                    }
                    mProgressText += "\n ======= " + num2 + " files addded =======";
                    mbProgressTextChanged = true;
                    if (num1 + num2 == 0)
                    {
                        // close database here so that it will not be copied in onPostExecute()
                        mDataBase.close();
                        mDataBase = null;
                    }
                    result = 0;
                    break;

                case scanCountAudioFiles:
                    result = runScanCountAudioFiles();
                    break;

                case checkDbFiles:
                    result = mDataBase.runSqlQueryCheckAllFiles(mFoundFilesMap);
                    break;

                case removeDbFiles:
                    mDataBase.removeDeletedAudioFilesFromDatabase();
                    mDataBase.removeChangedAudioFilesFromDatabase();
                    result = 0;
                    break;

                case addDbFiles:
                    for (Map.Entry<String, MusicDatabase.FoundAudioFile> entry : mFoundFilesMap.entrySet())
                    {
                        handleAudioFile(entry.getValue().df, true);
                    }
                    result = 0;
                    break;
            }
            return result;
        }


        /**************************************************************************
         *
         * AsyncTask method
         *
         *************************************************************************/
        @Override
        protected void onPostExecute(Integer result)
        {
            if ((actionCode == scanRebuildAll) || (actionCode == scanCountAudioFiles) || (actionCode == incremental))
            {
                Log.d(LOG_TAG, "doInBackground() : scanning done");
                mbProgressTextChanged = true;
                boolean bResult = true;
                boolean bSuccess = true;

                if ((mDataBase != null) && ((actionCode == scanRebuildAll) || (actionCode == incremental)))
                {
                    mDataBase.close();
                    mDataBase = null;
                    bSuccess = false;

                    // copy from internal memory to shared db path
                    if (mSharedDbPath != null)
                    {
                        bResult = copyDbToSafPath(mSharedDbPath, null, mSuggestedDbNameInSharedPath);
                        if (bResult)
                        {
                            mProgressText += "\n ======= Database File Copied To Shared Directory =======\n";
                            bSuccess = true;
                        } else
                        {
                            mProgressText += "\n ======= ERROR: Database file not copied to shared directory =======\n";
                        }
                    }
                    else
                    {
                        mProgressText += "\n ======= WARN: Database file not copied to shared path (none specified) =======\n";
                    }

                    // optional: copy from internal memory to music path
                    if (mbCopyDbToSafPath)
                    {
                        bResult = copyDbToSafPath(mDocumentFileTree, MainActivity.sDbPath, MainActivity.sDbName);
                        if (bResult)
                        {
                            mProgressText += "\n ======= Database file copied to music path =======\n";
                            bSuccess = true;
                        } else
                        {
                            mProgressText += "\n ======= ERROR: Database file not copied to music path =======\n";
                        }
                    }
                    else
                    {
                        mProgressText += "\n ======= WARN: Database file not copied to music path (disabled) =======\n";
                    }
                }

                long usedTime = currentTimeMillis() - mStartTime;
                usedTime /= 1000;       // -> seconds

                long h = usedTime / 3600;
                usedTime %= 3600;

                String stime = "";
                if (h > 0)
                {
                    stime = "" + h + 'h';
                }

                long m = usedTime / 60;
                usedTime %= 60;

                if (m > 0)
                {
                    stime += "" + m + '\'';
                }
                stime += "" + usedTime + "''";
                String finalText = (bSuccess) ? "DONE" : "FAILED";
                mProgressText += "\n ======= " + finalText + " (" + stime + ") =======\n";

                getMainActivity().tellScanResult((bResult) ? 0 : 1, mDocumentFileTree, mDatabaseFile);
            }
            else
            if (actionCode == checkDbFiles)
            {
                mProgressText += "\n ======= " + result + " deleted or changed files =======\n";
                mbProgressTextChanged = true;

                getMainActivity().tellScanResult(0, null, null);
            }
            else
            if (actionCode == removeDbFiles)
            {
                mProgressText += "\n ======= DONE =======\n";
                mbProgressTextChanged = true;

                getMainActivity().tellScanResult(0, null, null);
            }

            mRunningTask = null;        // signalise "not busy"
        }
    }
}
