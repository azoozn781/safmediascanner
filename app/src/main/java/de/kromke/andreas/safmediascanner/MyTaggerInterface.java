/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.safmediascanner;

import android.content.ContentResolver;
import android.util.Log;
import java.io.OutputStream;
import androidx.documentfile.provider.DocumentFile;
import de.kromke.andreas.audiotags.TagsBase;

@SuppressWarnings("Convert2Lambda")
public class MyTaggerInterface extends TaggerInterface
{
    private static final String LOG_TAG = "SMS : MyTagger";

    /******************************************************************************
     *
     * helper to check if directory name is CD<n>
     *
     * returns <n> or -1 in case of no match
     *
     *****************************************************************************/
    private static int checkDirNameIsCdNumber(String dirname)
    {
        if (dirname.toLowerCase().startsWith("cd"))
        {
            try
            {
                return Integer.decode(dirname.substring(2));
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        return -1;
    }


    public MusicDatabase.AudioFile getTags(final DocumentFile df, ContentResolver resolver)
    {
        TagsBase.doUseMovementTotal = false;    // do not show "1/4 Allegro", but "1. Allegro"
        TagsBase tags = TagsBase.fromDocumentFile(df, resolver);
        if (tags == null)
        {
            return null;
        }

        boolean res = tags.read();
        if (res)
        {
            tags.combineValues();
            tags.dump();

            // replace numerical genres with text
            tags.replaceNumericalGenres();

            // calculate duration, if possible
            int duration;
            if (tags.durationMs > 0)
            {
                duration = tags.durationMs;
            }
            else
            {
                duration = 0;
                Log.w(LOG_TAG, "readTagsFromUri() : unknown duration");
            }
            MusicDatabase.AudioFile af = new MusicDatabase.AudioFile();
            af.id = -1;               // add later
            af.track_no = 1000 * tags.getNumericValue(TagsBase.idDiscNo, 0) + tags.getNumericValue(TagsBase.idTrackNo, 0);

            af.title = tags.values[TagsBase.idCombinedMovement];    // automatically falls back to title, if necessary
            af.album = tags.values[TagsBase.idAlbum];
            af.album_id = -1;               // add later
            af.duration = duration;
            af.grouping = tags.values[TagsBase.idGrouping];
            af.subtitle = tags.values[TagsBase.idSubtitle];
            af.composer = tags.values[TagsBase.idComposer];
            af.performer = tags.values[TagsBase.idArtist];
            af.album_artist = tags.values[TagsBase.idAlbumArtist];
            af.conductor = tags.values[TagsBase.idConductor];
            af.genre = tags.values[TagsBase.idGenre];
            af.year = tags.getNumericValue(TagsBase.idYear, 0);
            af.uri = df.getUri().toString();
            af.pic_type = tags.coverPictureType;        // 0: none, 1: jpeg, 2: png
            af.pic_size = tags.coverPictureSize;
            af.tag_type = tags.tagType;                 // 0: unknown, 1: ID3v1, 2: ID3v2, 10: MP4, 11: Vorbis
            af.mtime = df.lastModified();

            // fallback for malformed year
            if ("2007-1-1".equals(tags.values[TagsBase.idYear]))
            {
                af.year = 2007;
                Log.w(LOG_TAG, "readTagsFromUri() : derived year \"" + af.year + "\" from tag value " + tags.values[TagsBase.idYear]);
            }

            // fallback for missing title
            if (af.title == null)
            {
                af.title = df.getName();
                if (af.title != null)
                {
                    int index = af.title.lastIndexOf('.');
                    if (index > 0)
                    {
                        af.title = af.title.substring(0, index);        // remove file name extension
                    }
                    Log.w(LOG_TAG, "readTagsFromUri() : derived title \"" + af.title + "\" from file name");
                }
                else
                {
                    Log.w(LOG_TAG, "readTagsFromUri() : could not derive title from file name");
                }
            }

            // fallback for missing information
            if (af.album == null)
            {
                DocumentFile dfp = df.getParentFile();
                if (dfp != null)
                {
                    af.album = dfp.getName();
                    if ((af.album != null) && (checkDirNameIsCdNumber(af.album) >= 0))
                    {
                        // path is like ".../Violinkonzerte/CD1/01-Allegro.mp3"

                        dfp = dfp.getParentFile();
                        if (dfp != null)
                        {
                            af.album = dfp.getName();
                        }
                    }
                }
                Log.w(LOG_TAG, "readTagsFromUri() : derived album \"" + af.album + "\" from directory name");
            }

            tags.close();
            return af;
        }
        else
        {
            tags.close();
            return null;
        }
    }

    int extractPicture
    (
        final DocumentFile dfAudioFile,
        final ContentResolver resolver,
        final String path,
        long length,
        long lastModified,
        final DocumentFile dfPicture
    )
    {
        TagsBase tags = TagsBase.fromDocumentFile(dfAudioFile, resolver);
        if (tags == null)
        {
            return -1;
        }

        // cover picture callback
        TagsBase.TagCallbacks cb = new TagsBase.TagCallbacks()
        {
            public int handlePicture(byte[] data, int index, int length, int pictureType, final String mimeType)
            {
                Log.d(LOG_TAG, "readTagsFromUri.cb() : found picture of type=" + pictureType + ", MIME type=" + mimeType + ", length=" + length);
                if ((pictureType == 3) && (dfPicture != null))
                {
                    // this is the cover picture

                    OutputStream outputStream;
                    try
                    {
                        outputStream = resolver.openOutputStream(dfPicture.getUri());
                        if (outputStream != null)
                        {
                            outputStream.write(data, index, length);
                            outputStream.close();
                        }
                        else
                        {
                            Log.e(LOG_TAG, "fromUri() : cannot open output stream");
                            return -2;
                        }
                    } catch (Exception e)
                    {
                        Log.e(LOG_TAG, "fromUri() : cannot open output stream, exception: " + e);
                        return -2;
                    }
                }
                return 0;
            }
        };
        tags.setPictureHandler(cb);
        boolean res = tags.read();
        tags.close();
        if (!res)
        {
            return -1;
        }

        return 0;
    }
}
