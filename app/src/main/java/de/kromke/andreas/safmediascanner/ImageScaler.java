/*
 * Copyright (C) 2018-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.safmediascanner;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import androidx.documentfile.provider.DocumentFile;

@SuppressWarnings("WeakerAccess")
public class ImageScaler
{
    private static final String LOG_TAG = "ImageScaler";

    private DocumentFile mOriginalFile;
    private DocumentFile mDirectory;
    public DocumentFile mScaledFile = null;
    private Bitmap mBitmap = null;
    private ContentResolver mResolver;
    private String mBaseName = null;

    int open(ContentResolver resolver, final DocumentFile df)
    {
        mResolver = resolver;
        mOriginalFile = df;
        mDirectory = mOriginalFile.getParentFile();
        InputStream is;
        try
        {
            is = mResolver.openInputStream(mOriginalFile.getUri());
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "open() : cannot get input stream: " + e);
            return -1;
        }
        mBitmap = BitmapFactory.decodeStream(is);
        mBaseName = df.getName();
        if ((mBitmap == null) || (mBaseName == null))
        {
            return -1;
        }

        int index = mBaseName.lastIndexOf('.');
        if (index > 0)
        {
            mBaseName = mBaseName.substring(0, index);
        }

        return 0;
    }

    // 0: unchanged, 1: changed, <0: error
    private int createDownscaledImageFile(int maxDim)
    {
        int ow = mBitmap.getWidth();
        int oh = mBitmap.getHeight();
        Log.d(LOG_TAG, "scale() : current size is w = " + ow + ", h = " + oh);

        if ((ow <= maxDim) && (oh <= maxDim))
        {
            return 0;       // nothing to do
        }

        int nw, nh;
        float ratio;
        if (ow >= oh)
        {
            nw = maxDim;
            ratio = oh;
            ratio /= ow;
            nh = (int) (nw * ratio);
        }
        else
        {
            nh = maxDim;
            ratio = ow;
            ratio /= oh;
            nw = (int) (nh * ratio);
        }

        Bitmap b2 = Bitmap.createScaledBitmap(mBitmap, nw, nh, false);
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        b2.compress(Bitmap.CompressFormat.JPEG,80 , outStream);

        int rc;
        OutputStream os = null;
        try
        {
            mScaledFile = mDirectory.createFile("image/jpeg", mBaseName + "_scaled.jpg");
            if (mScaledFile == null)
            {
                Log.e(LOG_TAG, "scale() : creating scaled image file failed.");
                return -1;
            }

            os = mResolver.openOutputStream(mScaledFile.getUri());
            if (os != null)
            {
                rc = 1;
            }
            else
            {
                rc = -2;
            }
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "open() : cannot get output stream: " + e);
            rc = -1;
        }

        if (rc >= 0)
        {
            // write data to file
            try
            {
                os.write(outStream.toByteArray());
                os.close();
            }
            catch (Exception e)
            {
                rc = -3;
            }
        }

        return rc;
    }


    @SuppressWarnings("SameParameterValue")
    int scale(int maxDim, boolean bKeepBackup)
    {
        int rc = createDownscaledImageFile(maxDim);
        if (rc > 0)
        {
            // scaled image has been created
            try
            {
                //
                // either rename old file or delete it
                //

                if (bKeepBackup)
                {
                    String fname = mOriginalFile.getName();
                    final String backupName = fname + ".backup";
                    if (!mOriginalFile.renameTo(backupName))
                    {
                        Log.e(LOG_TAG, "scale() : backup of original folder image file failed. Trying to delete.");
                        if (!mOriginalFile.delete())    // remove original file
                        {
                            Log.e(LOG_TAG, "scale() : deleting of original folder image file also failed.");
                            rc = -6;
                        }
                    }
                }
                else
                {
                    if (!mOriginalFile.delete())    // remove original file
                    {
                        Log.e(LOG_TAG, "scale() : deleting of original folder image file failed.");
                        rc = -6;
                    }
                }

                //
                // rename new file to "<basename>.jpg"
                //

                if (rc >= 0)
                {
                    final String finalFileName = mBaseName + ".jpg";
                    if (!mScaledFile.renameTo(finalFileName))
                    {
                        rc = -7;
                    }
                }
            }
            catch (Exception e)
            {
                rc = -8;
            }
        }

        return rc;
    }
}
