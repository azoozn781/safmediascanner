/*
 * Copyright (C) 2018-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.safmediascanner;

/*
 * minimalistic preferences
 */

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

public class MyPreferenceActivity extends AppCompatActivity
{
    static public class MySettingsFragment extends PreferenceFragmentCompat
    {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            setPreferencesFromResource(R.xml.preferences, rootKey);

            //
            // special handling for non-editable information about shared db path
            //

            Preference sharedDbPathPref = findPreference("prefSharedDbDirectoryUri");
            if (sharedDbPathPref != null)
            {
                final String key = sharedDbPathPref.getKey();
                PreferenceManager manager = getPreferenceManager();
                SharedPreferences sharedPrefs = manager.getSharedPreferences();
                String value = sharedPrefs.getString(key, "(none)");
                sharedDbPathPref.setSummary(value);

                /* edit text preferences have their value as text
                if (sharedDbPathPref instanceof EditTextPreference)
                {
                    //sharedDbPathPref.setSummary(sharedDbPathPref.getText());
                }
                 */
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new MySettingsFragment())
                .commit();
    }
}
