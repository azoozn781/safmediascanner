/*
 * Copyright (C) 2020-21 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.audiotags;

// -> https://xiph.org/flac/documentation_format_overview.html
// -> https://xiph.org/vorbis/doc/v-comment.html
// -> https://xiph.org/flac/format.html#def_STREAMINFO

// Note that Flac is a container format encapsulating e.g. Vorbis-encoded comments.


import android.util.Log;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@SuppressWarnings({"BooleanMethodIsAlwaysInverted", "SameParameterValue", "SpellCheckingInspection"})
public class TagsFlac extends TagsVorbis
{
    private static final String LOG_TAG = "TFLC";
    private boolean bIsLastBlock = false;


    // debug helper
    private String getBlockTypeStr(int blockType)
    {
        switch (blockType)
        {
            case 0:
                return "STREAMINFO";
            case 1:
                return "PADDING";
            case 2:
                return "APPLICATION";
            case 3:
                return "SEEKTABLE";
            case 4:
                return "VORBIS_COMMENT";
            case 5:
                return "CUESHEET";
            case 6:
                return "PICTURE";
            default:
                return "unknown";
        }
    }


    private class FlacHeader
    {
        private final byte[] data = new byte[4];

        public boolean read()
        {
            return readBytes(data);
        }

        public boolean check()
        {
            if ((data[0] != 'f') || (data[1] != 'L') || (data[2] != 'a') || (data[3] != 'C'))
            {
                Log.e(LOG_TAG, "FlacHeader::check() : missing fLaC signature");
                return false;
            }

            return true;
        }
    }


    private class FlacBlock
    {
        private final byte[] data = new byte[4];
        int blockType = -1;
        int payloadSize = 0;

        public boolean read()
        {
            return readBytes(data);
        }

        public boolean check()
        {
            if ((data[0] & 0x80) != 0)
            {
                Log.d(LOG_TAG, "FlacBlock::check() : this is the last metadata block");
                bIsLastBlock = true;
            }
            else
            {
                Log.d(LOG_TAG, "FlacBlock::check() : this is not the last metadata block");
            }

            blockType = data[0] & 0x7f;
            if (blockType == 127)
            {
                Log.e(LOG_TAG, "FlacBlock::check() : invalid block type " + blockType);
                return false;
            }
            Log.d(LOG_TAG, "FlacBlock::check() : block type is " + blockType + " (" + getBlockTypeStr(blockType) + ")");

            payloadSize = getBigEndianInt3(data, 1);
            Log.d(LOG_TAG, "FlacBlock::check() : payload size is " + payloadSize);

            return true;
        }

        boolean skipPayload()
        {
            return safeSkip(payloadSize);
        }

        boolean handleStreamInfo()
        {
            if (payloadSize < 34)
            {
                Log.e(LOG_TAG, "FlacBlock::handleStreamInfo() : payload size too small for stream info " + payloadSize);
                return false;
            }

            byte[] data2 = new byte[34];
            if (!readBytes(data2))
            {
                return false;
            }
            payloadSize -= data2.length;

            int minBlockSize  = getBigEndianInt2(data2, 0);   // minimum block size
            int maxBlockSize  = getBigEndianInt2(data2, 2);   // maximum block size
            int minFrameSize  = getBigEndianInt3(data2, 4);   // minimum frame size
            int maxFrameSize  = getBigEndianInt3(data2, 7);   // maximum frame size
            int sampleRate    = getBigEndianInt3(data2, 10) >> 4;   // sample rate in Hz (20 bits)
            int nChannels     = (getBigEndianInt1(data2, 12) >> 1) & 7;   // number of channels - 1
            int bitsPerSample = (getBigEndianInt2(data2, 12) >> 4) & 31;   // bits per sample - 1
            long totalSamples = (getBigEndianInt5(data2, 13)) & 68719476735L;   // number of samples in stream

            durationUnits = (int) ((totalSamples * 1000) / sampleRate);
            bitRate = sampleRate;

            Log.d(LOG_TAG, "FlacBlock::handleStreamInfo() :     -- minBlockSize = " + minBlockSize);
            Log.d(LOG_TAG, "FlacBlock::handleStreamInfo() :     -- maxBlockSize = " + maxBlockSize);
            Log.d(LOG_TAG, "FlacBlock::handleStreamInfo() :     -- minFrameSize = " + minFrameSize);
            Log.d(LOG_TAG, "FlacBlock::handleStreamInfo() :     -- maxFrameSize = " + maxFrameSize);
            Log.d(LOG_TAG, "FlacBlock::handleStreamInfo() :     -- sampleRate = " + sampleRate);
            Log.d(LOG_TAG, "FlacBlock::handleStreamInfo() :     -- nChannels = " + (nChannels + 1));
            Log.d(LOG_TAG, "FlacBlock::handleStreamInfo() :     -- bitsPerSample = " + (bitsPerSample + 1));
            Log.d(LOG_TAG, "FlacBlock::handleStreamInfo() :     -- totalSamples = " + totalSamples);
            Log.d(LOG_TAG, "FlacBlock::handleStreamInfo() :     --> duration = " + durationUnits + " ms");

            return true;
        }

        // header and picture data are read step by step
        boolean handlePicture()
        {
            if (payloadSize < 28)
            {
                Log.e(LOG_TAG, "FlacBlock::handlePicture() : payload size too small for picture " + payloadSize);
                return false;
            }

            byte[] data2 = new byte[8];
            if (!readBytes(data2))
            {
                return false;
            }
            payloadSize -= data2.length;

            int picType = getBigEndianInt4(data2, 0);   // picture type
            int strLen = getBigEndianInt4(data2, 4);    // MIME string length
            if (strLen > maxFrameSize)
            {
                Log.e(LOG_TAG, "FlacBlock::handlePicture() : MIME string length overflow " + strLen);
                return false;
            }

            // read MIME string and the following length of the description string
            data2 = new byte[strLen + 4];
            if (!readBytes(data2))
            {
                return false;
            }
            payloadSize -= data2.length;
            final String mimeType = new String(data2, 0, strLen, StandardCharsets.ISO_8859_1);
            Log.d(LOG_TAG, "FlacBlock::handlePicture() : MIME type = " + mimeType);

            strLen = getBigEndianInt4(data2, strLen);    // description string length
            if (strLen > maxFrameSize)
            {
                Log.e(LOG_TAG, "FlacBlock::handlePicture() : description string length overflow " + strLen);
                return false;
            }

            // read description string and the following data
            data2 = new byte[strLen + 20];
            if (!readBytes(data2))
            {
                return false;
            }
            payloadSize -= data2.length;

            final String pictureDescription = new String(data2, 0, strLen, StandardCharsets.UTF_8);
            Log.d(LOG_TAG, "FlacBlock::handlePicture() : description = " + pictureDescription);

            int picWidth = getBigEndianInt4(data2, strLen);
            int picHeight = getBigEndianInt4(data2, strLen + 4);
            int colourDepth = getBigEndianInt4(data2, strLen + 8);
            int numColours = getBigEndianInt4(data2, strLen + 12);
            int picSize = getBigEndianInt4(data2, strLen + 16);

            Log.d(LOG_TAG, "FlacBlock::handlePicture() : w = " + picWidth + ", h = " + picHeight + ", colour depth = " + colourDepth);
            Log.d(LOG_TAG, "FlacBlock::handlePicture() : num of colours = " + numColours);
            Log.d(LOG_TAG, "FlacBlock::handlePicture() : picture size = " + picSize);
            Log.d(LOG_TAG, "FlacBlock::handlePicture() : remaining payload = " + payloadSize);

            if (picSize > payloadSize)
            {
                Log.e(LOG_TAG, "FlacBlock::handlePicture() : picture size > payload size");
                return false;
            }
            if (picSize > maxFrameSize)
            {
                Log.e(LOG_TAG, "FlacBlock::handlePicture() : picture size overflow");
                return false;
            }

            // read picture
            data2 = new byte[picSize];
            if (!readBytes(data2))
            {
                return false;
            }
            payloadSize -= data2.length;

            rememberCoverPic(picType, picSize, mimeType);
            if (mHandlePictureCb != null)
            {
                int ret = mHandlePictureCb.handlePicture(data2, 0, picSize, picType, mimeType);
                //noinspection RedundantIfStatement
                if (ret < 0)
                {
                    return false;       // fatal error
                }
            }
            return true;
        }
    }


    public TagsFlac(InputStream is)
    {
        mInputStream = is;
        tagType = 11;            // Vorbis
    }


    public boolean read()
    {
        FlacHeader header = new FlacHeader();
        if (!header.read())
        {
            return false;
        }
        if (!header.check())
        {
            return false;
        }

        boolean bLookingForComments = true;
        boolean bLookingForPicture = (mHandlePictureCb != null);

        int i = 0;
        do
        {
            FlacBlock block = new FlacBlock();
            if (!block.read())
            {
                return false;
            }
            if (!block.check())
            {
                return false;
            }

            if ((i == 0) && (block.blockType != 0))
            {
                Log.e(LOG_TAG, "TagsFlac::read() : first block type must be STREAMINFO");
                return false;
            }

            if (block.blockType == 0)
            {
                // Streaminfo
                Log.d(LOG_TAG, "TagsFlac::read() : Picture found at file pos " + filePos);
                bLookingForPicture = false;
                if (!block.handleStreamInfo())
                {
                    return false;
                }
                if (!bLookingForComments)
                {
                    break;      // done
                }
                if (!block.skipPayload())       // usually the remaining payload should be 0
                {
                    return false;
                }
            }
            else
            if (block.blockType == 4)
            {
                // Vorbis comment
                Log.d(LOG_TAG, "TagsFlac::read() : Vorbis comment found at file pos " + filePos);
                long bytesLeftSaved = bytesLeft;
                bytesLeft = block.payloadSize;
                if (!readVorbisComment(null))
                {
                    return false;
                }
                bytesLeft = bytesLeftSaved;
                if (!bLookingForPicture)
                {
                    break;          // we just stop processing here
                }
                bLookingForComments = false;
            }
            else
            if (block.blockType == 6)
            {
                // Picture
                Log.d(LOG_TAG, "TagsFlac::read() : Picture found at file pos " + filePos);
                bLookingForPicture = false;
                if (!block.handlePicture())
                {
                    return false;
                }
                if (!bLookingForComments)
                {
                    break;      // done
                }
                if (!block.skipPayload())       // usually the remaining payload should be 0
                {
                    return false;
                }
            }
            else
            {
                if (!block.skipPayload())
                {
                    return false;
                }
            }
            i++;
        }
        while(!bIsLastBlock);

        return true;
    }
}
