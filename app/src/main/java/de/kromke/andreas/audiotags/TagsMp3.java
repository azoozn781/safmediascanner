/*
 * Copyright (C) 2020-21 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.audiotags;

// -> https://developer.android.com/reference/java/io/InputStream

// to get the duration of mp3 files:
//-> https://www.codeproject.com/Articles/8295/MPEG-Audio-Frame-Header


import android.util.Log;
import java.io.InputStream;


@SuppressWarnings("SpellCheckingInspection")
public class TagsMp3 extends TagsId3v2
{
    private static final String LOG_TAG = "TMP3";
    @SuppressWarnings("FieldCanBeLocal")
    private final boolean mbScanMp3;
    private boolean id3v1Found = false;
    private long fileSamples = 0;      // sum up number of samples
    private int fileSamplingRate = 0;   // common sampling rate, -1: different rates
    private long fileDurationUs = 0;    // sum of all frame durations, used for non-constant sampling rate
    private int fileBitRate = 0;
    private int numFramesFromXingHeader = -1;


    @SuppressWarnings("SpellCheckingInspection")
    private class Mp3Frame
    {
        private final byte[] data = new byte[4];
        //private final byte[] dataPrev = new byte[4];       // debug helper
        private final String[] audioVersionIdDbgTable =
        {
            "MPEG 2.5",
            "(reserved)",
            "MPEG 2",
            "MPEG 1"
        };
        private final String[] layerIndexDbgTable =
        {
            "(reserved)",
            "Layer III",
            "Layer II",
            "Layer I"
        };
        private final String[] channelModeDbgTable =
        {
            "Stereo", "Joint Stereo", "Dual channels", "Mono"
        };
        private final int[][] samplingRateTable =
        {
            // samplíng rate index 0 / 1 / 2 / 3
            {11025, 12000, 8000, -1},       // MPEG 2.5 (LSF)
            {-1, -1, -1, -1},               // reserved
            {22050, 24000, 16000, -1},      // MPEG 2 (LSF)
            {44100, 48000, 32000, -1}       // MPEG 1
        };
        private final int[][] bitRateTableMpeg1 =
        {
            // bit rate index 0 .. 15
            {-1, -1, -1, -1 -1, -1, -1 -1, -1, -1 -1, -1, -1 -1, -1, -1}, // reserved
            {0, 32, 40, 48,  56,  64,  80,  96, 112, 128, 160, 192, 224, 256, 320, -1}, // layer III
            {0, 32, 48, 56,  64,  80,  96, 112, 128, 160, 192, 224, 256, 320, 384, -1}, // layer II
            {0, 32, 64, 96, 128, 160, 192, 224, 256, 288, 320, 352, 384, 416, 448, -1}  // layer I
        };
        private final int[][] bitRateTableMpegOther =
        {
            // bit rate index 0 .. 15
            {-1, -1, -1, -1 -1, -1, -1 -1, -1, -1 -1, -1, -1 -1, -1, -1}, // reserved
            {0,  8, 16, 24, 32, 40, 48,  56,  64,  80,  96, 112, 128, 144, 160, -1}, // layer III
            {0,  8, 16, 24, 32, 40, 48,  56,  64,  80,  96, 112, 128, 144, 160, -1}, // layer II
            {0, 32, 48, 56, 64, 80, 96, 112, 128, 144, 160, 176, 192, 224, 256, -1}  // layer I
        };
        private final int[][] samplesPerFrameTable =
        {
            // resv / layer III / layer II / layer I
            {-1, 576, 1152, 384},       // MPEG 2.5 (LSF)
            {-1, -1, -1, -1},               // reserved
            {-1, 576, 1152, 384},      // MPEG 2 (LSF)
            {-1, 1152, 1152, 384}       // MPEG 1
        };
        private final int[] slotSizeTable = { -1, 1, 1, 4 }; // resvd, layer III, II, I

        int nFrames = 0;        // this object is reused, count number of frames here
        int samplingRate;
        int samplesPerFrame;
        int frameSize;
        int frameBitRate;
        int audioVersionId;
        int layerIndex;
        int channelMode;
        long frameDurationUs;   // for higher precision, use microseconds here
        boolean isXingHeader;


        public boolean readHeader()
        {
            isXingHeader = false;

            if (readAhead != null)
            {
                // copy existing data and append remaining bytes
                assert(data.length > readAhead.length);
                System.arraycopy(readAhead, 0, data, 0, readAhead.length);
                if (!readBytes(data, readAhead.length, data.length - readAhead.length))
                {
                    return  false;
                }
                readAhead = null;       // consumed
                return true;
            }
            else
            {
                return readBytes(data);
            }
        }

        // evaluate frame header
        public boolean check()
        {
            frameSize = 0;
            frameDurationUs = 0;

            if (((data[0] & 0xff) != 0xff) || ((data[1] & 0xe0) != 0xe0))
            {
                if (TagsId3v1.checkSignature(data))
                {
                    id3v1Found = true;
                    Log.d(LOG_TAG, "Mp3Frame::check() : ID3v1 Tag found instead of mp3 frame, stop scanning here");
                    return false;
                }

                Log.e(LOG_TAG, "Mp3Frame::check() : frame sync is invalid at file position " + filePos);
                return false;
            }

                audioVersionId = (data[1] & 0x18) >> 3;
                layerIndex = (data[1] & 0x06) >> 1;
            int protectionBit = (data[1] & 0x01);
            int bitrateIndex = (data[2] & 0xf0) >> 4;
            int samplingRateIndex = (data[2] & 0x0c) >> 2;
            int paddingBit = (data[2] & 0x02) >> 1;
            int privateBit = (data[2] & 0x01);
                channelMode = (data[3] & 0xc0) >> 6;
            int modeExtension = (data[3] & 0x30) >> 4;
            int copyrightBit = (data[3] & 0x08) >> 3;
            int originalBit = (data[3] & 0x04) >> 2;
            int emphasis = (data[3] & 0x03);

                samplingRate = samplingRateTable[audioVersionId][samplingRateIndex];
                samplesPerFrame = samplesPerFrameTable[audioVersionId][layerIndex];
            // bitRate is given in kBit, so already divided by 1000
            frameBitRate = (audioVersionId == 3) ? bitRateTableMpeg1[layerIndex][bitrateIndex] :
                    (audioVersionId >= 2) ? bitRateTableMpegOther[layerIndex][bitrateIndex] : -1;
            int slotSize = slotSizeTable[layerIndex];
            @SuppressWarnings("UnnecessaryLocalVariable") int paddingSize = paddingBit;

            // debug helper, prints first data and anytime if data change (without padding)
            if ((nFrames == 0)/* || (data[0] != dataPrev[0]) || (data[1] != dataPrev[1]) ||
                ((data[2] & 0xfd) != (dataPrev[2] & 0xfd)) || (data[3] != dataPrev[3])*/)
            {
                Log.d(LOG_TAG, "Mp3Frame::check() : frame #" + nFrames);
                Log.d(LOG_TAG, "Mp3Frame::check() :      audioVersionId = " + audioVersionId + " (" + audioVersionIdDbgTable[audioVersionId] + ")");
                Log.d(LOG_TAG, "Mp3Frame::check() :      layerIndex = " + layerIndex + " (" + layerIndexDbgTable[layerIndex] + ")");
                Log.d(LOG_TAG, "Mp3Frame::check() :      protectionBit = " + protectionBit);
                Log.d(LOG_TAG, "Mp3Frame::check() :      bitrateIndex = " + bitrateIndex);
                Log.d(LOG_TAG, "Mp3Frame::check() :      samplingRateIndex = " + samplingRateIndex);
                Log.d(LOG_TAG, "Mp3Frame::check() :      paddingBit = " + paddingBit);
                Log.d(LOG_TAG, "Mp3Frame::check() :      privateBit = " + privateBit);
                Log.d(LOG_TAG, "Mp3Frame::check() :      channelMode = " + channelMode + " (" + channelModeDbgTable[channelMode] + ")");
                Log.d(LOG_TAG, "Mp3Frame::check() :      modeExtension = " + modeExtension);
                Log.d(LOG_TAG, "Mp3Frame::check() :      copyrightBit = " + copyrightBit);
                Log.d(LOG_TAG, "Mp3Frame::check() :      originalBit = " + originalBit);
                Log.d(LOG_TAG, "Mp3Frame::check() :      emphasis = " + emphasis);
                Log.d(LOG_TAG, "Mp3Frame::check() :      -> samplingRate = " + samplingRate);
                Log.d(LOG_TAG, "Mp3Frame::check() :      -> samplesPerFrame = " + samplesPerFrame);
                Log.d(LOG_TAG, "Mp3Frame::check() :      -> bitRate = " + frameBitRate);
                Log.d(LOG_TAG, "Mp3Frame::check() :      -> slotSize = " + slotSize);
                Log.d(LOG_TAG, "Mp3Frame::check() :      -> paddingSize = " + paddingSize);

                //System.arraycopy(data, 0, dataPrev, 0, data.length);
            }

            if ((samplingRate <= 0) || (samplesPerFrame <= 0) || (frameBitRate <= 0) || (slotSize <= 0))
            {
                Log.e(LOG_TAG, "Mp3Frame::check() :      invalid layer or version or rates");
                return false;
            }
            frameSize = (((samplesPerFrame * frameBitRate * 1000) / (8 * samplingRate)) + paddingSize) * slotSize;

            if (nFrames == 0)
            {
                Log.d(LOG_TAG, "Mp3Frame::check() :      -> frameSize = " + frameSize + " bytes");
            }

            bytesLeft = frameSize - data.length;
            return true;
        }

        // first audio frame might be Xing header
        private boolean checkXingHeader()
        {
            if (layerIndex == 1 /*layer III*/)
            {
                final byte[] xing_data = new byte[12];

                int sideInfoSize = (audioVersionId == 3 /*MPEG 1*/) ?
                        ((channelMode == 3/*mono*/) ? 17 : 32) :
                        ((channelMode == 3/*mono*/) ? 9 : 17);
                if (bytesLeft < sideInfoSize + xing_data.length)
                {
                    return true;    // do nothing
                }

                // skip side information
                if (!safeSkip(sideInfoSize))
                {
                    return false;
                }

                // read and evaluate Xing info
                if (!readBytes(xing_data))
                {
                    return false;
                }

                if (((xing_data[0] == 'X') && (xing_data[1] == 'i') && (xing_data[2] == 'n') && (xing_data[3] == 'g')) ||
                    ((xing_data[0] == 'I') && (xing_data[1] == 'n') && (xing_data[2] == 'f') && (xing_data[3] == 'o')))
                {
                    isXingHeader = true;
                    int flags = getBigEndianInt4(xing_data, 4);
                    if ((flags & 1) != 0)
                    {
                        numFramesFromXingHeader = getBigEndianInt4(xing_data, 8);
                        Log.d(LOG_TAG, "checkXingHeader() : number of frames is " + numFramesFromXingHeader);
                    }
                }
            }

            return true;
        }

        // evaluate duration of frame
        @SuppressWarnings("BooleanMethodIsAlwaysInverted")
        public boolean calcDuration()
        {
            frameDurationUs = (samplesPerFrame * 1000000L + (samplingRate / 2)) / samplingRate;

            if (fileSamples == 0)
            {
                Log.d(LOG_TAG, "Mp3Frame::check() :      -> frameDuration = " + frameDurationUs / 1000 + " ms");
                fileDurationUs = frameDurationUs;
                fileSamplingRate = samplingRate;
                fileSamples = samplesPerFrame;
                fileBitRate = frameBitRate;
            } else
            {
                fileSamples += samplesPerFrame;
                fileDurationUs += frameDurationUs;
                if ((fileSamplingRate != samplingRate) && (fileSamplingRate >= 0))
                {
                    Log.w(LOG_TAG, "Mp3Frame::check() : frame sampling rate " + samplingRate + " differs from previous " + fileSamplingRate);
                    fileSamplingRate = -1;      // sampling rate differs between frames
                }
                if ((fileBitRate != frameBitRate) && (fileBitRate >= 0))
                {
                    Log.w(LOG_TAG, "Mp3Frame::check() : frame bit rate " + frameBitRate + " differs from previous " + fileBitRate);
                    fileBitRate = -1;      // sampling rate differs between frames
                }
            }

            return true;
        }

    } // private class Mp3Frame


    /**************************************************************************
     *
     * constructor
     *
     *************************************************************************/
    public TagsMp3(InputStream is, boolean doScanMp3)
    {
        super(is, false);
        mInputStream = is;
        mbScanMp3 = doScanMp3;
        tagType = 2;            // ID3v2
    }


    /**************************************************************************
     *
     * main loop:
     *
     *  read header
     *  loop
     *      read frame
     *  end loop
     *
     *************************************************************************/
    public boolean read()
    {
        //
        // read first three bytes to decide, if stream starts with ID3 tag
        //

        readAhead = new byte[3];
        if (!readBytes(readAhead))
        {
            return false;
        }
        boolean tagsFound = false;
        if (checkId3Signature(readAhead))
        {
            //
            // read ID3v2 tags, using base class
            //

            if (!super.read())
            {
                return false;
            }
            tagsFound = true;
        }

        if (tagType == 1)
        {
            Log.w(LOG_TAG, "read() : id3v1 found, this should not happen here.");
            return true;        // id3v1 data read
        }

        //
        // To evaluate the duration of the mp3 file, we must parse it completely
        // or at least the first audio header which might contain information.
        //
        // Also we must read the file in case we did not find Id3v2 and are looking for Id3v1
        //

        if ((mbScanMp3) || !tagsFound)
        {
            if (tagsFound && (bytesLeft > 0))
            {
                if (!safeSkip(bytesLeft))
                {
                    Log.w(LOG_TAG, "read() : cannot skip remaining Id3v2 bytes, giving up");
                    return true;
                }
            }

            // read frames until EOF
            Mp3Frame frame = new Mp3Frame();
            for (;;)
            {
                bytesLeft = Long.MAX_VALUE;     // the file is now the limit
                if (!frame.readHeader())
                {
                    break;
                }
                if (!frame.check())
                {
                    break;
                }
                if (frame.nFrames == 0)
                {
                    // the first audio frame can hold information
                    if (!frame.checkXingHeader())
                    {
                        break;
                    }
                    // shortcut: With Xing header, it is sufficient to read one audio frame
                    if (frame.isXingHeader)
                    {
                        // if the frame is a Xing header, skip it for duration calculation ...
                        if (tagsFound && doEvalMp3XingHeader && (numFramesFromXingHeader >= 0))
                        {
                            // ... or use it as the only frame for duration calculation, but not
                            // in case we are still searching for Id3v1 tags and the end of the file
                            if (!frame.calcDuration())
                            {
                                break;
                            }
                            Log.d(LOG_TAG, "read() : get file duration via Xing header");
                            fileSamples *= numFramesFromXingHeader;
                            break;    // shortcut: done
                        }
                        else
                        {
                            if (tagsFound)
                            {
                                Log.d(LOG_TAG, "read() : skip Xing header");
                            }
                            else
                            {
                                Log.d(LOG_TAG, "read() : skip Xing header while still hoping auf Id3v1 tags");
                            }
                        }
                    }
                    else
                    {
                        if (!frame.calcDuration())
                        {
                            break;
                        }
                    }
                }
                else
                {
                    frame.isXingHeader = false;
                    if (!frame.calcDuration())
                    {
                        break;
                    }
                }

                frame.nFrames++;

                if (!safeSkip(bytesLeft))
                {
                    break;
                }
            }

            // ignore id3v1 tags if we already have id3v2
            if (id3v1Found && !tagsFound && doCheckID3v1)
            {
                // there are four bytes in the frame header buffer that belong to id3v1
                TagsId3v1 tagsV1 = new TagsId3v1();
                // copy existing data and append remaining bytes
                assert(TagsId3v1.bufSize > frame.data.length);
                System.arraycopy(frame.data, 0, tagsV1.data, 0, frame.data.length);
                if (readBytes(tagsV1.data, frame.data.length, tagsV1.data.length - frame.data.length))
                {
                    if (tagsV1.check())
                    {
                        evalId3v1(tagsV1);
                    }
                }
                else
                {
                    Log.w(LOG_TAG, "read() : broken id3v1 tags found, ignore");
                }
            }

            // get duration either globally or summed up per frame
            if (fileSamplingRate > 0)
            {
                // all frames have same sampling rate
                Log.d(LOG_TAG, "read() : get file duration via number of samples (" + fileSamples + ") and sampling rate (" + fileSamplingRate + ")");
                durationUnits = (int) ((fileSamples * 1000 + (fileSamplingRate / 2)) / fileSamplingRate);
            }
            else
            {
                // summed up frame durations
                Log.w(LOG_TAG, "read() : get file duration via sum of frame durations, due to varying sampling rate.");
                durationUnits = (int) (fileDurationUs / 1000);
            }

            bitRate = fileBitRate * 1000;

            Log.d(LOG_TAG, "read() : " + frame.nFrames + " mp3 frames read.");
        }

        return true;
    }
}
